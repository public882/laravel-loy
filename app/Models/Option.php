<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use Blameable;

    protected $fillable = [
        'name','value'
    ];
}
