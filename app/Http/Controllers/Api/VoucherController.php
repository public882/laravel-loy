<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Voucher;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VoucherController extends Controller
{
    use ApiResponser;

    /**
     * Get list Voucher
     *
     * @group Voucher
     * @queryParam orderby string order record by column name (default: name). Example:id
     * @queryParam sort string sort record [ASC,DESC] (default: DESC). Example:DESC
     * @queryParam search string search by voouchee code. Example:

     */

    public function index(Request $request)
    {
        $orderby = $request->has('orderby') ? $request->get('orderby') : 'id';
        $sort = $request->has('sort') ? $request->get('sort') : 'DESC';

        $voucher = Voucher::query();
        if ($request->has('search')) {
            $search = $request->get('search');
            $column = ['code'];
            foreach ($column as $key => $col) {
                if ($key == 0) {
                    $voucher->where($col, 'like', '%' . $search . '%');
                } else {
                    $voucher->orWhere($col, 'like', '%' . $search . '%');
                }
            }
        }

        $voucher->orderBy($orderby, $sort);
        $result = $voucher->get();
        $total = $voucher->count();
        $data = $result->map(function ($item) {
            return $item;
        });
        $res['message'] = 'data found';
        $res['total'] = $total;
        $res['data'] = $data;
        return $this->successResponse($res);
    }

    /**
     * Get Voucher by id
     *
     * @group Voucher
     */
    public function show($id)
    {
        $voucher = Voucher::findOrFail($id);
        return $this->respond($voucher);
    }

    /**
     * Get Voucher by voucher code
     *
     * @group Voucher
     * @urlParam id string Voucher Code. Example: FREE
     */

    public function showCode($id)
    {
        $voucher = Voucher::where('code', $id)->first();
        if(!$voucher){
            return $this->failNotFound();
        }
        return $this->respond($voucher);
    }

    /**
     * Create Voucher
     *
     * <aside class="notice">only admin user can create Voucher.</aside>
     * @group Voucher
     * @authenticated
     */
    public function store(Request $request)
    {
        if (!Helper::isAdmin()) {
            return $this->failUnauthorized();
        }
        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            'discount' => 'required|numeric|min:0'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $fields = $request->all();
        $fields['code'] = $fields['code'];
        $fields['discount'] = $fields['discount'];
        $voucher = Voucher::create($fields);
        $message = 'create success';
        $id = $voucher->id;
        $voucherData = Voucher::findOrFail($id);
        return $this->respondCreated($voucherData, $message);
    }

    /**
     * Update Voucher by id
     *
     * <aside class="notice">only admin user can update Voucher.</aside>
     * @group Voucher
     * @authenticated
     */

    public function update(Request $request, $id)
    {
        $voucher = Voucher::findOrFail($id);
        if (!Helper::isAdmin()) {
            return $this->failUnauthorized();
        }
        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            'discount' => 'required|numeric|min:0'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $voucher->fill($request->all());
        $voucher->save();
        $message =  "update success";
        $voucherData = Voucher::findOrFail($id);
        return $this->respondUpdated($voucherData, $message);
    }

    /**
     * Delete Voucher by id
     *
     * <aside class="notice">only admin user can delete Voucher.</aside>
     * @group Voucher
     * @authenticated
     */

    public function delete($id)
    {
        $voucher = Voucher::findOrFail($id);
        if (!Helper::isAdmin()) {
            return $this->failUnauthorized();
        }
        $voucher->delete();
        $message = "delete success";
        return $this->respondDeleted($id, $message);
    }
}
