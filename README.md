
## Laravel 8

- update db configuration in file .env
- php artisan migrate.
- php artisan db:seed.
- php artisan key:generate


running server
- php artisan serve -> will run in 127.0.0.1:8000


default admin user
email: admin@site.com
password: admin

api docs
 - http://127.0.0.1:8000/docs
