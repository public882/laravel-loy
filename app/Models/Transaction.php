<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use Blameable, HasFactory, SoftDeletes;

    protected $fillable = [
        'invoice', 'address', 'payment', 'delivery', 'delivery_fee', 'user', 'voucher','code','discount','total','total_pay','status','seller', 'no_resi','notes'
    ];

    public function user($col = 'name')
    {
        $user = $this->belongsTo('App\Models\User', 'user');
        return $user->first();
    }
    public function seller($col = 'name')
    {
        $user = $this->belongsTo('App\Models\User', 'seller');
        return $user->first();
    }

    public function payment()
    {
        $payment = $this->belongsTo('App\Models\Payment', 'payment');
        $paymentData= $payment->first();
        $data['id']=$paymentData->id;
        $data['name']=$paymentData->name;
        $data['description']=$paymentData->description;
        return $data;
    }

    public function delivery()
    {
        $delivery = $this->belongsTo('App\Models\Delivery', 'delivery');
        $deliveryData = $delivery->first();
        $data['id'] = $deliveryData->id;
        $data['name'] = $deliveryData->name;
        $data['description'] = $deliveryData->description;
        return $data;
    }
}
