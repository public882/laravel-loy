@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('style')
<link href="{{asset('admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('AppDashboard') }}"> {{__('admin.dashboard') }}</a></li>
<li class="breadcrumb-item active">{{$page_title}}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('admin.settings.navigation',['tab'=>'localization'])
        <div class="card mb-3">
            <form action="{{ route('settings.localization') }}"  enctype="multipart/form-data" method="POST">
             @csrf
            <div class="card-body">
                {!!Helpers::displayAlert()!!}
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <form action="{{ route('settings.localization') }}" enctype="multipart/form-data" method="POST">
             @csrf
					<div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="col-form-label">{{ __('lang.default_language') }}</div>
                            <select style="width: 100%" class="shadow-none form-control select2 @error('default_language') is-invalid @enderror" id="default_language" name="default_language" required="">
                                <option value="" disabled selected>{{ __('lang.select',['name' => __('lang.default_language')]) }}</option>
                                @foreach ($list_languages as $language)
                                    <option value="{{ $language }}" {{ Str::lower(Helpers::option('language','en'))==Str::lower($language) ? "selected":"" }}>{{ Str::ucfirst($language) }}</option>
                                @endforeach
                            </select>
                            @error('default_language')
                                <div class="invalid-feedback" >
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="col-form-label">{{ __('lang.timezone') }}</div>
                            <select class="form-control select2 @error('timezone') is-invalid @enderror" id="timezone" name="timezone" required="">
                                <option value="" disabled selected>{{ __('lang.select',['name' => __('lang.timezone')]) }}</option>
                                @foreach (Helpers::timezones_list() as $key =>$item)
                                    <optgroup label="{{ $key }}">
                                        @foreach ($item as $timezone)
                                                <option value="{{ $timezone }}" {{ Helpers::option('timezone')==$timezone ? "selected":"" }} >{{ $timezone }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                            {{-- <input class="shadow-none form-control @error('timezone') is-invalid @enderror" type="text" id="timezone" value="{{ old('timezone') ?? Helpers::option('timezone')  }}" name="timezone" placeholder="{{ __('lang.timezone') }}">
                            @error('timezone')
                                <div class="invalid-feedback" >
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror --}}
                        </div>
					</div>
                     @can('update','settings')
                    <div class="form-footer">
                        <button type="submit" class="shadow-none  btn btn-primary">{{ __('lang.update') }}</button>
                    </div>
                    @endcan
                        </form>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
@section('script')
<script src="{{asset('admin/plugins/select2/js/select2.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.select2').select2();
    });
</script>
@endsection
