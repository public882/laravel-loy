<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory,Blameable,SoftDeletes;

    protected $fillable = [
        'name', 'description', 'price','image','qty','status'
    ];

    public function user($col = 'name')
    {
        $user = $this->belongsTo('App\Models\User', 'created_by');
        return $user->first();
    }
}
