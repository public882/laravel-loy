@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('style')
<link href="{{asset('admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('users.index') }}"> {{__('admin.users') }}</a></li>
<li class="breadcrumb-item active">{{$page_title}}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card mb-3">
            <form action="{{ isset($user) ? route('users.update',['user'=>$user->id]) : route('users.store') }}" enctype="multipart/form-data" method="POST">
                <div class="card-body">
                    @if (isset($user))
                        @method('PATCH')
                    @endif
                    @csrf
                    {!!Helpers::displayAlert()!!}
                    <div class="row">
                        <div class="form-group col-md-5 col-sm-12">
                            <div class="form-group mb-3">
                                <input class="form-control shadow-none @error('name') is-invalid @enderror" type="text" id="name" value="{{ old('name') ?? (isset($user) ? $user->name:'')  }}" name="name" required="" placeholder="{{ __('admin.name') }}" autocomplete="off">
                                @error('name')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <input class="form-control shadow-none @error('phone') is-invalid @enderror" type="text" id="phone" value="{{ old('phone') ?? (isset($user) ? $user->phone:'') }}" name="phone" placeholder="{{ __('admin.phone') }}" autocomplete="off">
                                @error('phone')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <input class="form-control shadow-none @error('email') is-invalid @enderror" type="email" id="email" value="{{ old('email') ?? (isset($user) ? $user->email:'') }}" name="email" required="" placeholder="{{ __('E-Mail Address') }}" autocomplete="off">
                                @error('email')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <input class="form-control shadow-none @error('password') is-invalid @enderror" type="password" id="password" value="" name="password" {{ isset($user) ? '': 'required' }} placeholder="{{ __('Password') }}">
                                @if (isset($user))
                                <small id="passwordHelp" class="form-text text-muted">({{ trans('admin.leave_empty_not_to_change') }})</small>
                                @endif
                                @error('password')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <select class="w-100 form-control shadow-none select2 @error('role') is-invalid @enderror" id="role" name="role" required="">
                                    <option value="" disabled selected>{{ __('admin.select',['name' => __('admin.roles')]) }}</option>
                                    @foreach ($roles as $item)
                                    <option value="{{ $item->id }}" {{ isset($user) && $user->role==$item->id ? "selected" : '' }}>{{ ucwords($item->name) }}</option>
                                    @endforeach
                                </select>
                                @error('role')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="form-footer text-right">
                                <button class="btn btn-primary px-4 shadow-none">{{ __('admin.save') }}</button>
                            </div>
                        </div>
                        <div class="form-group col-md-7 col-sm-12 border-left">
                            @php
                                $permissionsData['user']=isset($user) ? $user : null;
                            @endphp
                            @include('admin.users.permissions',$permissionsData)
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end row -->
@endsection

@section('script')
<script src="{{asset('admin/plugins/select2/js/select2.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.select2').select2();
    });
    $(function() {
        $('select[name="role"]').on('change', function() {
            var roleid = $(this).val();
            var url = '<?php echo route("users.role_changed",["role"=>":id"]) ;?>';
            url = url.replace(':id', roleid);
            if (roleid == '' || roleid == '1') {
                $('.roles').find('input').prop('disabled', true).prop('checked', false);
            } else {
                $('.roles').find('input').prop('disabled', false).prop('checked', false);
            }

            change_roles_permissions(url, roleid, true);
        });
    })
</script>
@endsection
