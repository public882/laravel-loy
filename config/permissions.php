<?php
return[
    'roles'=>[
        'name'=>'Roles',
        'access'=>[
            'view'   => 'View Own',
            'viewAny'   => 'View All',
            'create' => 'Create',
            'update'   => 'Update',
            'delete' => 'Delete',
        ]
    ],
    'users'=>[
        'name'=>'Users',
        'access'=>[
            'view'   => 'View Own',
            'viewAny'   => 'View All',
            'create' => 'Create',
            'update'   => 'Update',
            'delete' => 'Delete',
        ]
    ],
    'settings'=>[
        'name'=>'Settings',
        'access'=>[
            'view'   => 'View Own',
            'viewAny'   => 'View All',
            'create' => 'Create',
            'update'   => 'Update',
            'delete' => 'Delete',
        ]
    ],

];
