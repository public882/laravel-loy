<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use HasFactory,Blameable,SoftDeletes;

    protected $fillable = [
        'channel', 'sender', 'reciever','updated_at'
    ];

    public function sender($col = 'name')
    {
        $user = $this->belongsTo('App\Models\User', 'sender');
        return $user->first();
    }

    public function reciever($col = 'name')
    {
        $user = $this->belongsTo('App\Models\User', 'reciever');
        return $user->first();
    }
}
