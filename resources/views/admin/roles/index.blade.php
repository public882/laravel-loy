@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title',  $page_title)

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('AppDashboard') }}"> {{__('admin.dashboard') }}</a></li>
<li class="breadcrumb-item active">{{$page_title}}</li>
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('admin-style/plugins/datatables/datatables.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('admin-style/plugins/sweet-alert/sweetalert2.css')}}">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card mb-3">
            <div class="card-header">
                <span class="pull-left">
                    @can('create','roles')
                    <a href="{{ route('roles.create') }}" class="shadow-none btn btn-primary btn-sm text-white px-3" ><i class="fas fa-user-check" aria-hidden="true"></i> {{ __('admin.new',['name'=>__('admin.roles')]) }}</a>
                    @endcan
                </span>
            </div>
            <div class="card-body">
                 {!!Helpers::displayAlert()!!}
                 <div class="table-responsive mb-3">
                     <table class="table table-hover display table-loading" id="data-table">
                        <thead class="display-none">
                            <tr>
                                <th scope="col" style="width:5%">#</th>
                                <th scope="col">{{ __('admin.roles') }}</th>
                                <th scope="col">{{ __('admin.users') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no=1;
                            @endphp
                            @foreach ($roles as $role)
                            @php
                                $row_action=[];
                            @endphp
                            @can('update','roles')
                            @php
                            $row_action[] = [
                                'title' => __('admin.edit'),
                                'class' => 'text-primary',
                                'link'  => route('roles.edit',['role'=>$role->id]),
                            ];
                            @endphp
                            @endcan
                            @if ($role->user()->count()<1)
                                @can('delete','roles')
                                @php
                                    $row_action[] = [
                                        'title' => __('admin.delete'),
                                        'class' => 'text-danger',
                                        'link'  => route('roles.destroy',['role'=>$role->id]),
                                        'onclick'=>"deleteData(".$role->id.")"
                                    ];
                                @endphp
                                @endcan
                            @endif
                            <tr>
                                <td><b>{{ $no++ }}</b></td>
                                <td scope="row">
                                    <b>{{ $role->name }}</b>
                                    @if ($role->id !='1')
                                        @include('admin.layout.action',['link'=>$row_action])
                                        <form name="formDelete{{ $role->id }}" id="formDelete{{ $role->id }}" action="{{ route('roles.destroy',['role'=>$role->id]) }}" method="POST">
                                            @method("DELETE")
                                            @csrf
                                        </form>
                                    @endif
                                </td>
                                <td>{{ number_format($role->user()->count()) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                     </table>
                 </div>
            </div>
        </div>
    </div>
</div>
{{-- @include('admin.layout.filemanager') --}}
<!-- end row -->
@endsection

@section('script')
<script src="{{asset('admin-style/plugins/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin-style/plugins/sweet-alert/sweetalert.min.js')}}"></script>
<script>
     $(document).ready(function() {
        $('#data-table').DataTable();
    });
    function deleteData(dat){
        swal({
            title: '{{ __("admin.confirm")}}',
            icon: 'warning',
            buttons: {
                cancel: {
                    text: "{{ __('admin.no')}}",
                    value: null,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                },
                confirm: {
                    text: "{{ __('admin.yes')}}",
                    value: true,
                    visible: true,
                    className: "btn btn-primary",
                    closeModal: false
                }
            },
            closeOnEsc: false,
            closeOnClickOutside: false,
        }).then((value) => {
            if (value === true) {
                document.getElementById("formDelete"+dat).submit();
            }
        }).catch(err => {
            if (err) {
                swal("Oops!", " {{ __('admin.error_message') }}", "error");
            } else {
                swal.stopLoading();
                swal.close();
            }
        });
    }
</script>
@endsection
