<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'registration_success' => 'Registration Success, Your account is already active. Please login',
    'registration_need_admin_confirmation' => 'Registration Success, Your Account will active when admin accept it',
    'email_verification_notification' => 'We have sent a verification email to you, please confirm to verify your email',
    'email_verification_success' => 'Email verification success, your account is active. Please login',
    'deleteDone' => 'Data was deleted successfully',
    'addDone' => 'New record has been Added successfully',
    'saveDone' => 'Modifications you have made saved successfully',
    'not_found' => 'Data not found',
    'change_role_permission_warning' => 'Changing role permissions won\'t affected current users members permissions that are using this role.',
];
