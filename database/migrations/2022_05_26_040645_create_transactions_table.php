<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('invoice')->nullable();
            $table->longText('address')->nullable();
            $table->unsignedBigInteger('payment')->nullable();
            $table->unsignedBigInteger('delivery')->nullable();
            $table->integer('delivery_fee')->default(0);
            $table->unsignedBigInteger('seller')->nullable();
            $table->unsignedBigInteger('user')->nullable();
            $table->unsignedBigInteger('voucher')->nullable();
            $table->string('code')->nullable();
            $table->integer('discount')->default(0);
            $table->double('total')->default(0);
            $table->double('total_pay')->default(0);
            $table->string('no_resi')->nullable();
            $table->text('notes')->nullable();
            $table->enum('status', ['pending payment', 'process','send','cancel','complain','done'])->default('pending payment');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
