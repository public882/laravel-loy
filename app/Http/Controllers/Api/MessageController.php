<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DetailMessage;
use App\Models\Message;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    use ApiResponser;

    /**
     * Get List Message
     *
     * @group Chat / Messages
     * @authenticated
     */

    public function messages(){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $messages=Message::where('sender', $user->id)->orWhere('reciever', $user->id)->orderBy('updated_at','desc')->get();
        foreach($messages as $key=>$message){
            $dataUnread=DetailMessage::select('id')->where('channel', $message['channel'])->where('user','!=', $user->id)->where('is_read', '!=',1)->get();
            $unread=count($dataUnread);
            $lastMessage='';
            $last_message = DetailMessage::where('channel', $message['channel'])->orderBy('created_at', 'desc')->first();
            if($last_message){
                $lastMessage=$last_message->content;
            }
            $message['sender']=$message->sender();
            $message['reciever']=$message->reciever();
            $message['unread']= $unread;
            $message['last_message']= $lastMessage;
        }
        $total = count($messages);
        $result = $messages;
        $res['message'] = 'data found';
        $res['total'] = $total;
        $res['data'] = $result;
        return $this->successResponse($res);
    }

    /**
     * Get List Detail Message by Channel Id
     *
     * @group Chat / Messages
     * @authenticated
     * @urlParam id string Cannel ID of message. Example: 9921120373726879e3122007
     */

    public function detailMessage(Request $request,$id){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $messages = Message::where('channel',$id)->where(function ($query) {
            $user = Auth::user();
            $query->where('sender', '=', $user->id)->orWhere('reciever', '=', $user->id);
        })->first();

        if (!$messages) {
            return $this->failNotFound();
        }
        $detail = DetailMessage::where('channel', $id)->orderBy('created_at', 'desc');
        $result = $detail->get();
        foreach ($result as $key => $message) {
            $message['user'] = $message->user();
        }
        $total = $detail->count();
        DetailMessage::where('channel', $id)->where('user','!=',$user->id)->update(['is_read' => 1]);
        $res['message'] = 'data found';
        $res['total'] = $total;
        $res['data'] = $result;
        return $this->successResponse($res);
    }

    /**
     * Post / Send Message /  Chat
     *
     * @group Chat / Messages
     * @authenticated
     *
     * @bodyParam reciever string required ID of user target.
     * @bodyParam message longtext required Content / Text message
     */

    public function postMessage(Request $request){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $validator = Validator::make($request->all(), [
            'reciever' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        if($user->id == $request->reciever){
            return $this->failNotFound();
        }
        User::findOrFail($request->reciever);
        $channel= $this->createChannel($user->id, $request->reciever);
        if(!$channel){
            return $this->failValidationError('create channel fail');
        }
        $fields['message'] = $channel->id;
        $fields['channel'] = $channel->channel;
        $fields['content'] = $request->message;
        $fields['user'] = $user->id;
        $detailMessage = DetailMessage::create($fields);
        $channelField['updated_at']= $detailMessage->updated_at;
        $channel->fill($channelField);
        $channel->save();
        $message = 'send message success';
        return $this->respondCreated($detailMessage, $message);
    }

    private function createChannel($sender,$reciever){
        $user = Auth::user();
        if (!$user) {
            return false;
        }
        $message=Message::where(function($query) use ($sender, $reciever){
            $query->where('sender', '=', $sender)->where('reciever', '=', $reciever);
        })->orWhere(function ($query) use ($sender, $reciever) {
            $query->where('reciever', '=', $sender)->where('sender', '=', $reciever);
        })->first();
        if($message){
            return $message;
        }else{
            $fields['channel']=sha1(time()).$sender.$reciever;
            $fields['sender']= $sender;
            $fields['reciever']= $reciever;
            $mess=Message::create($fields);
            return $mess;
        }
    }
}
