<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    use Blameable, HasFactory, SoftDeletes;

    protected $fillable = [
        'code', 'discount'
    ];
}
