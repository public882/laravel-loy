<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory,Blameable;

    protected $fillable = [
        'product', 'qty','user','seller'
    ];

    public function product()
    {
        $product = $this->belongsTo('App\Models\Product', 'product');
        return $product->first();
    }
    public function user()
    {
        $user = $this->belongsTo('App\Models\User', 'user');
        return $user->first();
    }
}
