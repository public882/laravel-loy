<script src="{{ asset('admin-style/js/modernizr.min.js') }}"></script>
<script src="{{ asset('admin-style/js/jquery.min.js') }}"></script>
<script src="{{ asset('admin-style/js/moment.min.js') }}"></script>

<script src="{{ asset('admin-style/js/popper.min.js') }}"></script>
<script src="{{ asset('admin-style/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('admin-style/js/detect.js') }}"></script>
<script src="{{ asset('admin-style/js/fastclick.js') }}"></script>
<script src="{{ asset('admin-style/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('admin-style/js/jquery.nicescroll.js') }}"></script>

<!-- App js -->
<script src="{{ asset('admin-style/js/admin.js') }}"></script>
<script src="{{ asset('admin-style/js/main.js') }}"></script>
