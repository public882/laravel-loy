<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $db = new Role();
        $db->name = 'admin';
        $db->access = null;
        $db->updated_at = now();
        $db->created_at = now();
        $db->save();

        $db = new Role();
        $db->name = 'user';
        $db->access = null;
        $db->updated_at = now();
        $db->created_at = now();
        $db->save();
    }
}
