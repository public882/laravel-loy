<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    use ApiResponser;

    /**
     * Get list product
     *
     * @group Products
     * @queryParam offset int offset of record (default: 0). Example:0
     * @queryParam limit int limit of record (default: 10). Example:10
     * @queryParam orderby string order record by column name (default: id). Example:id
     * @queryParam sort string sort record [ASC,DESC] (default: DESC). Example:DESC
     * @queryParam status string show product for active status[all=>all,1=>active,0=>inactive] (default: 1). Example:1
     * @queryParam search string search by product name or product description. Example:
     */

    public function index(Request $request){
        $offset = $request->has('offset') ? $request->get('offset') : 0;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $orderby = $request->has('orderby') ? $request->get('orderby') : 'id';
        $sort = $request->has('sort') ? $request->get('sort') : 'DESC';
        $status = $request->has('status') ? $request->get('status') : '1';

        $product = Product::query();
        $product->skip($offset)->take($limit);
        if ($request->has('search')) {
            $search = $request->get('search');
            $column = ['name','description'];
            foreach ($column as $key => $col) {
                if ($key == 0) {
                    $product->where($col, 'like', '%' . $search . '%');
                } else {
                    $product->orWhere($col, 'like', '%' . $search . '%');
                }
            }
        }
        if($status!='all'){
            $product->where('status', $status);
        }
        $product->orderBy($orderby, $sort);
        $result = $product->get();
        $total = $product->count();
        $data = $result->map(function ($item) {
            $item['user']=$item->user();
            return $item;
        });
        $res['message'] = 'data found';
        $res['total'] = $total;
        $res['data'] = $data;
        return $this->successResponse($res);
    }

    /**
     * Get product by id
     *
     * @group Products
     */

    public function show(Request $request, $id)
    {
        $status = $request->has('status') ? $request->get('status') : '1';
        $product = Product::findOrFail($id);
        if ($status != 'all' && $product->status!=1) {
            return $this->failNotFound("data not found");
        }
        $product['user'] = $product->user();
        return $this->respond($product);
    }

    /**
     * Create product
     *
     * @group Products
     * @authenticated
     * @bodyParam description string data description. Example: Baju untuk Hangout
     * @bodyParam image string image url. Example: https://static.semrush.com/blog/uploads/media/00/6e/006eebc38b54220916caecfc80fed202/Guide-to-URL-Parameters-2.png
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'price' => 'required|numeric|min:0',
            'qty' => 'numeric|min:0'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $fields = $request->all();
        $fields['name'] = $fields['name'];
        if(isset($fields['description'])){
            $fields['description'] = $fields['description'];
        }
        if(isset($fields['image'])){
            $fields['image'] = $fields['image'];
        }
        $fields['price'] = $fields['price'];
        $product = Product::create($fields);
        $message ='create success';
        $id = $product->id;
        $productData = Product::findOrFail($id);
        return $this->respondCreated($productData, $message);
    }

    /**
     * Update product by id
     *
     * @group Products
     * @authenticated
      * @bodyParam description string data description. Example: Baju untuk Hangout
     * @bodyParam image string image url. Example: https://static.semrush.com/blog/uploads/media/00/6e/006eebc38b54220916caecfc80fed202/Guide-to-URL-Parameters-2.png
     * @bodyParam status boolean status active or inactive. true=>active, false=>inactive. Example:1
     */

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $user = Auth::user();
        if(!Helper::isAdmin() && $user->id != $product->created_by){
            return $this->failNotFound();
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'price' => 'required|numeric|min:0',
            'qty' => 'numeric|min:0',
            'status' => 'boolean'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $product->fill($request->all());
        $product->save();
        $message = $message = "update success";
        $productData = Product::findOrFail($id);
        return $this->respondUpdated($productData, $message);
    }

    /**
     * Delete product
     *
     * @group Products
     * @authenticated
     */

    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $user = Auth::user();
        if (!Helper::isAdmin() && $user->id != $product->created_by) {
            return $this->failNotFound();
        }
        $product->delete();
        $message ="delete success";
        return $this->respondDeleted($id, $message);
    }
}
