<?php

use App\Helpers\Helper;
use Illuminate\Support\Facades\Route;

Route::Group(
    [
        'prefix' => Helper::option('user_path', 'user'),
        'middleware' => Helper::option('verify_email') === '1' ? ['verified', 'auth'] : ['auth']
    ],
    function () {
        Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('UserDashboard');
    }
);
