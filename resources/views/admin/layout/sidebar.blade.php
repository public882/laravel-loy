
<div class="left main-sidebar">

    <div class="sidebar-inner leftscroll">

        <div id="sidebar-menu">

            <ul>

                <li class="submenu">
                    <a class="{{ Route::currentRouteName()=='AppDashboard' ? 'active' :'' }} subdrop" href="{{route('AppDashboard')}}">
                        <i class="fas fa-bars"></i>
                        <span>{{__('admin.dashboard')}}</span>
                    </a>
                </li>
                @if (Gate::allows('accessView', 'roles') || Gate::allows('accessView', 'users') || Gate::allows('accessView', 'settings'))
                <div class="h6 mt-4 p-3 text-white bg-dark"><span>{{ trans('admin.setup') }}</span></div>
                @endif
                @can('accessView', 'users')
                @php
                    $currentFolder = "users"; // Put folder name here
                    $PathCurrentFolder = substr($urlPath, 0, strlen($currentFolder));

                @endphp
                <li class="submenu">
                    <a class="{{ $PathCurrentFolder==$currentFolder ? 'active' : '' }}" href="{{route('users.index')}}">
                        <i class="fas fa-users"></i>
                        <span>{{ trans('admin.users') }}</span>
                    </a>
                </li>
                @endcan
                @can('accessView', 'roles')
                @php
                    $currentFolder = "roles"; // Put folder name here
                    $PathCurrentFolder = substr($urlPath, 0, strlen($currentFolder));
                @endphp
                <li class="submenu">
                    <a class="{{ $PathCurrentFolder==$currentFolder ? 'active' : '' }}" href="{{route('roles.index')}}">
                        <i class="fas fa-user-check"></i>
                        <span>{{ trans('admin.roles') }}</span>
                    </a>
                </li>
                @endcan
                 @can('accessView', 'settings')
                <?php
                $currentFolder = "settings"; // Put folder name here
                $PathCurrentFolder = substr($urlPath, 0, strlen($currentFolder));
                ?>
                <li class="submenu">
                    <a class="{{ $PathCurrentFolder==$currentFolder ? 'active' : '' }}" href="{{route('settings.index')}}">
                        <i class="fas fa-cogs"></i>
                        <span>{{ trans('admin.settings') }}</span>
                    </a>
                </li>
                @endcan
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="clearfix"></div>

    </div>

</div>
<!-- End Sidebar -->
