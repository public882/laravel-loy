<?php
namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{

    public function successResponse($data=null,$code=Response::HTTP_OK){

        return response()->json($data,$code)->header('Content-Type','application/json');
    }

    public function errorResponse($message=null,$code=Response::HTTP_BAD_REQUEST){
        return response()->json(['error'=>$code,'message'=>$message],$code);
    }

    public function errorMessage($message=null,$code){
        return response()->json($message,$code)->header('Content-Type','application/json');
    }

    public function response($data=null,string $message='response ok',$code=Response::HTTP_OK){

        return response()->json(['message'=>$message,$data],$code);
    }
    public function respond($data=null,string $message='response ok',$code=Response::HTTP_OK){
        $res['message']=$message;
        $res['data']=$data;
        return response()->json($res,$code);
    }

    public function fail($message=null,$code=Response::HTTP_BAD_REQUEST){
        return response()->json(['error'=>$code,'message'=>$message],$code);
    }

    public function failValidator($message = 'Bad Request', $code = Response::HTTP_BAD_REQUEST)
    {
        return response()->json(['error' => $code, 'message' => $message], $code);
    }


    public function respondCreated($data=null,$message='',$code=Response::HTTP_CREATED){
        return $this->respond($data,$message,$code);
    }

    public function respondDeleted($data=null,$message='',$code=Response::HTTP_OK){
        return $this->respond($data,$message,$code);
    }

    public function respondUpdated($data=null,$message='',$code=Response::HTTP_OK){
        return $this->respond($data,$message,$code);
    }

    public function respondNoContent($data=null,$message='',$code=Response::HTTP_NO_CONTENT){
        return $this->respond($data,$message,$code);
    }

    public function failUnauthorized(string $message='Unauthorized' ,$code=Response::HTTP_UNAUTHORIZED){
        return $this->fail($message,$code);
    }

    public function failForbidden(string $message='Forbidden' ,$code=Response::HTTP_FORBIDDEN){
        return $this->fail($message,$code);
    }

    public function failNotFound(string $message='Not Found' ,$code=Response::HTTP_NOT_FOUND){
        return $this->fail($message,$code);
    }

    public function failValidationError($message='Bad Request' ,$code=Response::HTTP_BAD_REQUEST){
        return $this->fail($message,$code);
    }

    public function failServerError(string $message='Internal Server Error' ,$code=Response::HTTP_INTERNAL_SERVER_ERROR){
        return $this->fail($message,$code);
    }
}
