@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('AppDashboard') }}"> {{__('admin.dashboard') }}</a></li>
<li class="breadcrumb-item active">{{$page_title}}</li>
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('admin-style/plugins/datatables/datatables.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('admin-style/plugins/sweet-alert/sweetalert2.css')}}">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card mb-3">
            <div class="card-header">
                <span class="pull-left">
                    @can('create','users')
                    <a href="{{ route('users.create') }}" class="shadow-none btn btn-primary btn-sm text-white px-3" ><i class="fas fa-user-plus" aria-hidden="true"></i> {{ __('admin.new',['name'=>__('admin.users')]) }}</a>
                    @endcan
                </span>
            </div>
            <div class="card-body">
                 {!!Helpers::displayAlert()!!}
                 <div class="table-responsive mb-3">
                    <table class="table table-hover display table-loading" id="data-table">
                        <thead class="display-none">
                            <tr>
                                <th scope="col" style="width:5%">#</th>
                                <th scope="col">{{ __('admin.name') }}</th>
                                <th scope="col">{{ __('E-mail Address') }}</th>
                                <th scope="col">{{ __('admin.roles') }}</th>
                                <th scope="col">{{ __('admin.status') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
@section('script')
<script src="{{asset('admin-style/plugins/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin-style/plugins/sweet-alert/sweetalert.min.js')}}"></script>
<script>
    var datatable=$("#data-table").DataTable({
        autoWidth: false,
        processing:true,
        serverside:true,
        ordering:true,
        ajax:{
            url:'{!! url()->current() !!}'
        },
        columns:[
            {data:'id',name:'id',width:'6%'},
            {data:'displayName',name:'displayName'},
            {data:'email',name:'email',width:'35%'},
            {data:'role',name:'role',width:'15%'},
            {data:'status',name:'status',width:'10%'},
        ],

    })
</script>
<script>
    function deleteData(dat){
        swal({
            title: '{{ __("admin.confirm")}}',
            icon: 'warning',
            buttons: {
                cancel: {
                    text: "{{ __('admin.no')}}",
                    value: null,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true,
                },
                confirm: {
                    text: "{{ __('admin.yes')}}",
                    value: true,
                    visible: true,
                    className: "btn btn-primary",
                    closeModal: false
                }
            },
            closeOnEsc: false,
            closeOnClickOutside: false,
        }).then((value) => {
            if (value === true) {
                document.getElementById("formDelete"+dat).submit();
            }
        }).catch(err => {
            if (err) {
                swal("Oops!", err, "error");
            } else {
                swal.stopLoading();
                swal.close();
            }
        });
    }
</script>
@endsection
