<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailMessage extends Model
{
    use HasFactory,Blameable;

    protected $fillable = [
        'channel', 'message', 'user','content', 'is_read'
    ];

    public function user($col = 'name')
    {
        $user = $this->belongsTo('App\Models\User', 'user');
        return $user->first();
    }
}
