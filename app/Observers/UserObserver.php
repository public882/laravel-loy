<?php

namespace App\Observers;

use App\Models\User;
use App\Helpers\Helper;
use App\Http\Controllers\Admin\UserController;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        // $default_role = Helper::defaultRole();
        $user->email_verified_at = Helper::option('verify_email', '0') == '1' ? NULL : now();
        $user->status = Helper::option('auto_accept_user_registration', '1') == '1' ? '1': '0';
        // $user->role = $default_role ? $default_role->id: NULL;
        $user->save();
        $userController = new UserController;
        // $permissions = unserialize($default_role->access);
        // $userController->update_permissions($permissions, $user->id);

        if (Helper::option('verify_email') === '1') {
            Helper::alert(__('alert.email_verification_notification'));
        } else {
            if (Helper::option('auto_accept_user_registration') === '1') {
                Helper::alert(__('alert.registration_success'));
            } elseif (Helper::option('auto_accept_user_registration') === '0') {
                Helper::alert(__('alert.registration_need_admin_confirmation'));
            }
        }

    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
       if($user->wasChanged('email_verified_at') && $user->getOriginal('email_verified_at')==NULL ){
            Helper::alert(__('alert.email_verification_success'));
       }
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
