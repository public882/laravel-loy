
        <!-- top bar navigation -->
        <div class="headerbar">

            <!-- LOGO -->
            <div class="headerbar-left">
                <a href="{{ route('AppDashboard') }}" class="logo">
                    <img alt="Logo" src="{{ asset('admin-style/images/logo.png') }}" />
                    <span>{{__('admin.panel')}}</span>
                </a>
            </div>

            <nav class="navbar-custom">

                <ul class="list-inline float-right mb-0">
                    <li class="list-inline-item dropdown notif">
                        <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" aria-haspopup="false" aria-expanded="false">
                            <img src="{{ asset('admin-style/images/avatars/admin.png') }}" alt="Profile image" class="avatar-rounded"> <small>{{ Auth::user()->name }}</small>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                            <a href="{{route('profile')}}" class="dropdown-item notify-item">
                                <i class="fas fa-user"></i>
                                <span>{{ __('admin.profile') }}</span>
                            </a>

                            <!-- item-->
                            <a href="{{ route('logout') }}" class="dropdown-item notify-item"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="fas fa-power-off"></i>
                                <span>{{ __('admin.logout') }}</span>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </a>
                        </div>
                    </li>

                </ul>

                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left">
                            <i class="fas fa-bars"></i>
                        </button>
                    </li>

                    <li class="float-left d-none d-sm-block">
                        <a class="button-menu-mobile" target="_blank" title="Go to website" href="/">
                            <i class="fas fa-laptop"></i>
                        </a>
                    </li>
                </ul>

            </nav>

        </div>
        <!-- End Navigation -->
