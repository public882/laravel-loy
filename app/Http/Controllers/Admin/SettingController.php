<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        Helper::authorize('accessView','settings');
        $data['title'] = __('admin.settings');
        return view('admin.settings.index', $data);
    }

    public function general(Request $request){
        Helper::authorize('update', 'settings');
        Option::where('name', 'site_url')->first()->update([
            'value' => $request->site_url
        ]);
        Option::where('name', 'site_keyword')->first()->update([
            'value' => $request->site_keyword
        ]);
        Option::where('name', 'site_description')->first()->update([
            'value' => $request->site_description
        ]);
        Option::where('name', 'site_title')->first()->update([
            'value' => $request->site_title
        ]);
        Helper::alert(__('alert.saveDone'));
        return redirect()->route('settings.index');
    }

    public function email(Request $request){
        if ($request->isMethod('get')) {
            Helper::authorize('accessView', 'settings');
            $data['title'] = __('lang.email');
            return view('admin.settings.email', $data);
        }
        if ($request->isMethod('post')) {
            Helper::authorize('update', 'settings');
            Option::where('name','mail_driver')->first()->update([
                'value'=>$request->mail_driver
            ]);
            Option::where('name','mail_host')->first()->update([
                'value'=>$request->mail_host
            ]);
            Option::where('name','mail_port')->first()->update([
                'value'=>$request->mail_port
            ]);
            Option::where('name','mail_username')->first()->update([
                'value'=>$request->mail_username
            ]);
            Option::where('name','mail_password')->first()->update([
                'value'=>$request->mail_password
            ]);
            Option::where('name','mail_encryption')->first()->update([
                'value'=>$request->mail_encryption
            ]);
            Option::where('name','mail_from_address')->first()->update([
                'value'=>$request->mail_sender
            ]);
            Option::where('name','mail_from_name')->first()->update([
                'value'=>$request->mail_sender_name
            ]);
            Helper::alert(__('alert.saveDone'));
            return redirect()->route('settings.email.get');
        }
    }

    public function authentication(Request $request){
        if ($request->isMethod('get')) {
            Helper::authorize('accessView', 'settings');
            $data['title'] = __('lang.authentication');
            return view('admin.settings.authentication', $data);
        }
        if ($request->isMethod('post')) {
            Helper::authorize('update', 'settings');
            Option::where('name','verify_email')->first()->update([
                'value'=>$request->verify_new_user_email
            ]);
            Option::where('name','default_role')->first()->update([
                'value'=>$request->default_user_role
            ]);
            Option::where('name','register')->first()->update([
                'value'=>$request->allow_registration
            ]);
            Option::where('name','auto_accept_user_registration')->first()->update([
                'value'=>$request->auto_accept_user_registration
            ]);
            Helper::alert(__('alert.saveDone'));
            return redirect()->route('settings.authentication.get');
        }
    }
    public function localization(Request $request){
        if ($request->isMethod('get')) {
            Helper::authorize('accessView', 'settings');
            $data['title'] = __('lang.localization');
            $lang_path= resource_path('lang');
            $directories = [];
            $items = scandir($lang_path);
            foreach ($items as $item) {
                if ($item == '..' || $item == '.')
                continue;
                if (is_dir($lang_path . '/' . $item))
                $directories[] = $item;
            }
            $data['list_languages'] = $directories;
            return view('admin.settings.localization', $data);
        }
        if ($request->isMethod('post')) {
            Helper::authorize('update', 'settings');
            Option::where('name', 'language')->first()->update([
                'value' => $request->default_language
            ]);
            Option::where('name', 'timezone')->first()->update([
                'value' => $request->timezone
            ]);
            Helper::alert(__('alert.saveDone'));
            return redirect()->route('settings.localization.get');
        }
    }

    public function managesite(Request $request)
    {
        if ($request->isMethod('get')) {
            Helper::authorize('accessView', 'settings');
            $data['maintenance'] = Helper::option('maintenance')=='1' ? true:false;
            $data['title'] = __('lang.site_status');
            return view('admin.settings.status', $data);
        }

        if ($request->isMethod('post')) {
            Helper::authorize('update', 'settings');
            Option::where('name', 'maintenance')->first()->update([
                'value' => $request->site_status
            ]);
            Helper::alert(__('alert.saveDone'));
            return redirect()->route('settings.menagesite.get');
        }
    }
}
