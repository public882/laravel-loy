<nav class="nav nav-tabs" id="myTab" role="tablist">
<a class="nav-item nav-link {{ $tab=='general' ?'active':'' }}" href="{{ route('settings.index') }}"><i class="fas fa-cog" aria-hidden="true"></i> {{ __('lang.general') }}</a>
<a class="nav-item nav-link {{ $tab=='email' ?'active':'' }}" href="{{ route('settings.email.get') }}"><i class="fas fa-envelope" aria-hidden="true"></i> {{ __('lang.email') }}</a>
<a class="nav-item nav-link {{ $tab=='authentication' ?'active':'' }}" href="{{ route('settings.authentication.get') }}"><i class="fas fa-user-plus" aria-hidden="true"></i> {{ __('lang.authentication') }}</a>
<a class="nav-item nav-link {{ $tab=='localization' ?'active':'' }}" href="{{ route('settings.localization.get') }}"><i class="fas fa-globe" aria-hidden="true"></i> {{ __('lang.localization') }}</a>
<a class="nav-item nav-link {{ $tab=='status' ?'active':'' }}" href="{{ route('settings.menagesite.get') }}"><i class="fas fa-laptop-code" aria-hidden="true"></i> {{ __('lang.site_status') }}</a>
</nav>
