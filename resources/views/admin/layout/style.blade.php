 <link rel="shortcut icon" href="{{ asset('admin-style/images/favicon.ico') }}">
<!-- Bootstrap CSS -->
<link href="{{ asset('admin-style/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Font Awesome CSS -->
<link href="{{ asset('admin-style/font-awesome/css/all.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom CSS -->
<link href="{{ asset('admin-style/css/style.css') }}" rel="stylesheet" type="text/css" />
