<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Custom credentials to validate the status of user.
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password', 'status');
    }

    protected function attemptLogin(Request $request)
    {
        $request->request->add(['status' => '1']);
        return $this->guard()->attempt(
                $this->credentials($request),
                $request->filled('remember')
            );
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'exists:users,' . $this->username() . ',status,1',
            'password' => 'required|string',
        ], [
            $this->username() . '.exists' => __('auth.login_email')
        ]);
    }
}
