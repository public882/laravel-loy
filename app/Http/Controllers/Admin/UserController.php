<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    protected function validator(array $data, $id = NULL)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['min:6'],
            'email' => ['required', 'string', 'email', 'max:255', $id ? Rule::unique('users')->ignore($id, 'id') : 'unique:users'],
            'password' => $id ? [] : ['required', 'string', 'min:5'],
        ]);
    }

    public function profile(){
        $data['title'] = __('lang.profile');
        return view('admin.users.profile', $data);
    }

    public function update_profile(Request $request){
        $user = User::find(Auth::user()->id);
        if (empty($user)) {
            return redirect()->route('AppDashboard');
        }
        $request->validate([
            'name' => ['required', 'max:255'],
            'phone' => ['min:6'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user->id, 'id')],
        ]);
        if (!empty($request->password)) {
            $request->validate([
                'password' => ['required', 'min:5'],
            ]);
            $user->password = Hash::make($request->password);
        }
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->save();
        Helper::alert(__('alert.saveDone'));
        return redirect()->route('profile');
    }

    public function update_permissions($permissions = [], $id = NULL)
    {
        Permission::where('user_id', $id)->delete();
        if (is_array($permissions)) {
            foreach ($permissions as $key => $value) {
                foreach ($value as $item) {
                    Permission::create([
                        'user_id' => $id,
                        'module' => $key,
                        'access' => $item,
                    ]);
                }
            }
        }
        return true;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Helper::authorize('delete', 'users');
        if ($request->ajax()) {
            $query=User::query();
            return DataTables::of($query)
                ->addColumn('displayName',function($rows){
                $row_action = [];
                $formDelete='';
                if (Auth::user()->can('update', 'users') && $rows->id != 1) {
                    $row_action[] = [
                        'title' => __('admin.edit'),
                        'class' => 'text-info',
                        'link' => route('users.edit', ['user' => $rows->id]),
                    ];
                }
                if (Auth::user()->can('update', 'users') && $rows->id != 1) {
                    if ($rows->status == '1') {
                        $row_action[] = [
                            'title' => __('admin.deactivate'),
                            'class' => 'text-danger',
                            'link'  => route('users.deactivate', ['user' => $rows->id]),
                        ];
                    } elseif ($rows->status == '0') {
                        $row_action[] = [
                            'title' => __('admin.activate'),
                            'class' => 'text-success',
                            'link'  => route('users.activate', ['user' => $rows->id]),
                        ];
                    }
                }
                if (Auth::user()->can('delete', 'users') && $rows->id != 1) {

                    $row_action[] = [
                        'title' => __('admin.delete'),
                        'class' => 'text-danger',
                        'link'  => route('users.destroy', ['user' => $rows->id]),
                        'onclick' => "deleteData(" . $rows->id . ")"
                    ];
                    $formDelete = "<form name='formDelete" . $rows->id . "' id='formDelete" . $rows->id . "' action='" . route('users.destroy', ['user' => $rows->id]) . "' method='POST'>";
                    $formDelete .= method_field('delete');
                    $formDelete .= csrf_field();
                    $formDelete .= "</form>";
                }
                return "<b>" . ucwords($rows->name) . "</b>".Helper::action_links($row_action) . $formDelete;
                })
                ->editColumn('role', function ($rows) {
                    return $rows->roles();
                })
                ->editColumn('status', function ($rows) {
                    if ($rows->status == '1') {
                        $status_user = "<label class='badge badge-success'>" . __('active') . "</label>";
                    } elseif ($rows->status == '0') {
                        $status_user = "<label class='badge badge-danger'>" . __('inactive') . "</label>";
                    }
                    return $status_user;
                })
            ->rawColumns(['displayName', 'roles', 'status'])
            ->make();
        }
        $data['title'] = __('admin.users');
        return view('admin.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Helper::authorize('create', 'users');
        $data['title']=__('admin.new',['Name'=>__('admin.users')]);
        $data['roles'] = Role::all();
        return view('admin.users.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Helper::authorize('create', 'users');
        $this->validator($request->all())->validate();
        $create = User::factory()->createQuietly([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'email_verified_at' => now(),
            'status' => '1',
            'role' => $request['role'],
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'password' => Hash::make($request['password']),
        ]);
        $this->update_permissions($request->permissions, $create->id);
        Helper::alert(__('alert.addDone'));
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Helper::authorize('update', 'users');
        $users = User::find($id);
        if (!$users || $id == '1') {
            Helper::alert(__('admin.not_found'), 'danger');
            return redirect()->route('users.index');
        }
        $data['user'] = $users;
        $data['title'] = __('admin.update') . ' ' . __('admin.users') . ' : ' . $users->name;
        $data['roles'] = Role::all();
        return view('admin.users.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Helper::authorize('update', 'users');
        $users = User::find($id);
        if (!$users || $id == '1') {
            Helper::alert(__('admin.not_found'), 'danger');
            return redirect()->route('users.index');
        }
        $this->validator($request->all(), $id)->validate();
        $users->name = $request->name;
        $users->phone = $request->phone;
        $users->email = $request->email;
        $users->role = $request->role;
        if (!empty($request->password)) {
            $users->password = Hash::make($request->password);
        }
        $users->save();
        $this->update_permissions($request->permissions, $id);
        Helper::alert(__('alert.saveDone'));
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Helper::authorize('delete', 'users');
        $users = User::find($id);
        if (!$users || $users == '1') {
            Helper::alert(__('admin.not_found'), 'danger');
            return redirect()->route('users.index');
        }
        Permission::where('user_id', $id)->delete();
        User::destroy($id);
        Helper::alert(__('alert.deleteDone'));
        return redirect()->route('users.index');
    }

    public function deactivate($id = NULL, Request $request)
    {
        Helper::authorize('update', 'users');
        $user = User::find($id);
        if (empty($user)) {
            Helper::alert(__('admin.not_found'), 'danger');
            return redirect()->route('users.index');
        }

        $user->status = '0';
        $user->save();
        Helper::alert(__('alert.saveDone'));
        return redirect()->route('users.index');
    }
    public function activate($id = NULL, Request $request)
    {
        Helper::authorize('update', 'users');
        $user = User::find($id);
        if (empty($user)) {
            Helper::alert(__('admin.not_found'), 'danger');
            return redirect()->route('users.index');
        }

        $user->status = '1';
        $user->save();
        Helper::alert(__('alert.saveDone'));
        return redirect()->route('users.index');
    }

    public function role_changed($id = NULL)
    {

        $check_permission = Role::find($id);
        $check_permission->access = !empty($check_permission->access) ? unserialize($check_permission->access) : [];
        echo json_encode($check_permission->access);
    }

}
