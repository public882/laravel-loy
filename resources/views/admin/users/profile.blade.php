@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('AppDashboard') }}">{{ __('lang.dashboard') }}</a></li>
<li class="breadcrumb-item active">{{$page_title }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card mb-3">
            <div class="card-body">
                {!!Helpers::displayAlert()!!}
                <div class="row">
					<div class="form-group col-md-5 col-sm-12 mb-3">
                        <form action="{{ route('update_profile') }}" enctype="multipart/form-data" method="POST">
                            @csrf

                        <div class="form-group mb-3">
                            <label for="{{ __('lang.name') }}" class="form-label">{{ __('lang.name') }}</label>
                            <input class="shadow-none form-control @error('name') is-invalid @enderror" type="text" id="name" value="{{ old('name') ?? Auth::user()->name }}" name="name" placeholder="{{ __('lang.name') }}">
                            @error('name')
                                <div class="invalid-feedback" >
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="{{ __('lang.phone') }}" class="form-label">{{ __('lang.phone') }}</label>
                            <input class="shadow-none form-control @error('phone') is-invalid @enderror" type="text" id="phone" value="{{ old('phone') ?? Auth::user()->phone }}" name="phone" required="" placeholder="{{ __('lang.phone') }}">
                            @error('phone')
                                <div class="invalid-feedback" >
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="{{ __('lang.email') }}" class="form-label">{{ __('lang.email') }}</label>
                            <input class="shadow-none form-control @error('email') is-invalid @enderror" type="text" id="email" value="{{ old('email') ?? Auth::user()->email }}" name="email" required="" placeholder="{{ __('lang.email') }}">
                            @error('email')
                                <div class="invalid-feedback" >
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="{{ __('Password') }}" class="form-label">{{ __('Password') }}</label>
                            <input class="shadow-none form-control @error('password') is-invalid @enderror" type="text" id="password" value="" name="password" placeholder="{{ __('Password') }}">
                            <small id="passwordHelp" class="form-text text-muted">({{ trans('lang.leave_empty_not_to_change') }})</small>
                            @error('password')
                                <div class="invalid-feedback" >
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>

                        <div class="form-footer text-md-right">
                            <button type="submit" class="btn btn-primary px-4 w-100 shadow-none">{{ trans('lang.update') }}</button>
                        </div>
                    </form>
                    </div>
                    {{-- <div class="form-group col-md-7 col-sm-12 border-left">
                        <div id="avatar_image" class="text-center">
                            <form action="{{ route('update_avatar') }}" enctype="multipart/form-data" method="POST">
                            <p>
                            <img class="border rounded h-100 mt-2" style="width:180px;" alt="{{ Auth::user()->displayName() }}" src="{{action('AssetsController@avatar',['user', Auth::user()->id])}}" />
                            </p>
                            <p>
                            <a class='btn btn-sm btn-success btn-file-upload text-white'>
                                    {{ __('lang.select',['name' => __('lang.image')]) }}
                                    <input type="file" name="image" size="100" accept=".png, .jpg, .jpeg" onchange="$('#file-data').html($(this).val().replace(/.*[\/\\]/, ''));" required>
                                </a>
                                <br>
                                <small id="passwordHelp" class="form-text text-muted">({{ trans('lang.size') }} {{ CommonHelper::GetOption('avatar_width') .'px * '.CommonHelper::GetOption('avatar_height') .'px' }}) </small>
                                <span class='badge badge-primary' id="file-data"></span>
                                @error('image')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </p>

                            @csrf
                                <button class="btn btn-sm btn-primary px-4 shadow-none">{{ trans('lang.update') }} {{ trans('lang.image') }}</button>
                            </form>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
