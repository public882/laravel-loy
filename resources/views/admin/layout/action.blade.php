@php
$init = '';
$act = [];
if(isset($link) and is_array($link)){
foreach ((array)$link as $actions) {
    $title = 'link';
    $class = 'text-info';
    $link = '#';
    $onclick = '';
    if (array_key_exists("title", $actions)) {
        $title = $actions['title'];
    }
    if (array_key_exists("class", $actions)) {
        $class = $actions['class'];
    }
    if (array_key_exists("link", $actions)) {
        $link = $actions['link'];
    }
    if (array_key_exists("onclick", $actions)) {
        $link = 'javascript::';
        $onclick = 'onclick="' . $actions['onclick'] . '"';
    }
    $act[] = "<a class='" . $class . "' href='" . $link . "'" . $onclick . ">" . $title . "</a>";
}
$init     = implode(" | ", $act);
}
@endphp
<div class="row-option"><?php echo $init ;?></div>
