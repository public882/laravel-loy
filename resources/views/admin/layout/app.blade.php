<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title',Helpers::option('site_title',@env('APP_NAME')))</title>
    <meta name="description" content="">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    @include('admin.layout.style')
    @yield('style')
</head>

<body class="adminbody">

    <div id="main">


        @include('admin.layout.header')
        @include('admin.layout.sidebar')

        <div class="content-page">
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="breadcrumb-holder">
                                <h1 class="main-title float-left">@yield('title')</h1>
                                <ol class="breadcrumb float-right">
                                    @yield('breadcrumb')
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    @yield('content')
                    <!-- end row -->

                </div>
                <!-- END container-fluid -->

            </div>
        </div>
        <!-- END content-page -->
        @include('admin.layout.footer')
        @include('admin.layout.script')
        @yield('script')
    </div>
    <!-- END main -->

</body>

</html>
