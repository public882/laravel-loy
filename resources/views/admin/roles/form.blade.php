@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('roles.index') }}"> {{__('admin.roles') }}</a></li>
<li class="breadcrumb-item active">{{$page_title}}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card mb-3">
            <form action="{{ isset($role) ? route('roles.update',['role'=>$role->id]) : route('roles.store') }}" enctype="multipart/form-data" method="POST">
                <div class="card-body">
                    @if (isset($role))
                        @method('PATCH')
                    @endif
                    @csrf
                    {!!Helpers::displayAlert()!!}
                    <div class="row">
                        <div class="form-group col-md-9 col-sm-12">
                            @if (isset($role) && $role->user()->count()>0)
                            <div class="alert alert-info dark mb-3">
                                <div class="col-form-label">{{ __('alert.change_role_permission_warning') }}</div>
                                <div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
                                    <div class="checkbox checkbox-info">
                                        <input id="update_user_permissions" type="checkbox" name="update_user_permissions" value="1" data-original-title="" title="">
                                        <label class="mb-0" for="update_user_permissions"><b>{{ __('admin.role_update_user_permissions') }}</b></label>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="form-group mb-3">
                                <input class="form-control @error('role_name') is-invalid @enderror" type="text" id="role_name" value="{{ isset($role) ? $role->name : old('role_name') }}" name="role_name" required="" placeholder="{{ __('admin.role_name') }}">
                                @error('role_name')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            @php
                                $permissionsData['role']=isset($role) ? $role : null;
                            @endphp
                            @include('admin.users.permissions',$permissionsData)
                            <div class="form-footer text-right">
                                <button class="btn btn-primary px-4 shadow-none">{{ __('admin.save') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
