<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Option;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *s
     * @return void
     */
    public function run()
    {
        $db=new Option();
        $db->name= 'maintenance';
        $db->value='0';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='language';
        $db->value='en';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db = new Option();
        $db->name = 'admin_path';
        $db->value = 'admin';
        $db->updated_at = now();
        $db->created_at = now();
        $db->save();

        $db = new Option();
        $db->name = 'user_path';
        $db->value = 'user';
        $db->updated_at = now();
        $db->created_at = now();
        $db->save();

        $db=new Option();
        $db->name='timezone';
        $db->value='UTC';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='register';
        $db->value='0';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='default_role';
        $db->value='user';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='auto_accept_user_registration';
        $db->value='1';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='verify_email';
        $db->value='1';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='mail_driver';
        $db->value='smtp';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='mail_host';
        $db->value='';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='mail_port';
        $db->value='587';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='mail_encryption';
        $db->value='tls';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='mail_username';
        $db->value='';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='mail_password';
        $db->value='';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='mail_from_address';
        $db->value='';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='mail_from_name';
        $db->value='';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='site_title';
        $db->value='';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='site_description';
        $db->value='';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='site_keyword';
        $db->value='';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='site_url';
        $db->value='';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='avatar_width';
        $db->value='300';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

        $db=new Option();
        $db->name='avatar_height';
        $db->value='300';
        $db->updated_at=now();
        $db->created_at=now();
        $db->save();

    }
}
