<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'panel' => 'Admin Panel',
    'hello' => 'Hello',
    'profile' => 'Profile',
    'logout' => 'Logout',
    'dashboard' => 'Dashboard',
    'setup' => 'Setup',
    'users' => 'Users',
    'roles' => 'Roles',
    'settings' => 'Settings',
    'new' => 'New :Name',
    'edit' => 'Edit',
    'save' => 'Save',
    'delete' => 'Delete',
    'update' => 'Update',
    'confirm' => 'Are You sure ?',
    'yes' => 'Yes',
    'no' => 'No',
    'error_message' => 'Something wrong',
    'role_is_used' => ':number users use this role',
    'role_update_user_permissions' => 'Update user permission who use this role',
    'role_name' => 'Role name',
    'feature' => 'Feature',
    'access' => 'Access',
    'name' => 'Name',
    'status' => 'Status',
    'deactivate' => 'Deactivate',
    'activate' => 'Activate',
    'phone' => 'Phone',
    'leave_empty_not_to_change' => 'Leave empty not to change',
    'select' => 'Select :Name',
    'general' => 'General',
];
