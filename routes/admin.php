<?php

use App\Helpers\Helper;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\Dashboard;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::Group(
    [
        'prefix' => Helper::option('admin_path', 'admin'),
        'middleware' => Helper::option('verify_email') === '1' ? ['verified', 'auth'] : ['auth']
    ],
    function () {
        Route::get('/', [Dashboard::class, 'index'])->name('AppDashboard');
        Route::resource('roles', RoleController::class);
        Route::resource('users', UserController::class);
        Route::get('users/deactivate/{user}', [UserController::class, 'deactivate'])->name('users.deactivate');
        Route::get('users/activate/{user}', [UserController::class, 'activate'])->name('users.activate');
        Route::get('users/role_changed/{role}', [UserController::class, 'role_changed'])->name('users.role_changed');
        Route::get('settings', [SettingController::class, 'index'])->name('settings.index');
        Route::post('settings/general', [SettingController::class, 'general'])->name('settings.general');
        Route::get('settings/email', [SettingController::class, 'email'])->name('settings.email.get');
        Route::post('settings/email', [SettingController::class, 'email'])->name('settings.email');
        Route::get('settings/authentication', [SettingController::class, 'authentication'])->name('settings.authentication.get');
        Route::post('settings/authentication', [SettingController::class, 'authentication'])->name('settings.authentication');
        Route::get('settings/localization', [SettingController::class, 'localization'])->name('settings.localization.get');
        Route::post('settings/localization', [SettingController::class, 'localization'])->name('settings.localization');
        Route::get('settings/menage-site', [SettingController::class, 'managesite'])->name('settings.menagesite.get');
        Route::post('settings/menage-site', [SettingController::class, 'managesite'])->name('settings.managesite');
        Route::get('profile', [UserController::class, 'profile'])->name('profile');
        Route::post('profile', [UserController::class, 'update_profile'])->name('update_profile');

    }
);
