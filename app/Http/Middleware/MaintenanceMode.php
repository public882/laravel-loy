<?php

namespace App\Http\Middleware;

use App\Helpers\Helper;
use Closure;
use Illuminate\Http\Request;

class MaintenanceMode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $path_exception=[
            Helper::option('admin_path', 'admin'),
            Helper::option('admin_path', 'admin').'/*',
            'login',
            'password/*',
            'logout',
            'home',
        ];

        if(Helper::option('maintenance','0')=='1' && !$request->is($path_exception)){
            abort(503);
        }
        return $next($request);
    }
}
