<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory, Blameable;

    protected $fillable = [
        'transaction', 'product', 'seller', 'user', 'rating','comment'
    ];

    public function trans($col = 'name')
    {
        $user = $this->belongsTo('App\Models\Transaction', 'transaction');
        return $user->first();
    }
    public function user($col = 'name')
    {
        $user = $this->belongsTo('App\Models\User', 'user');
        return $user->first();
    }
    public function seller($col = 'name')
    {
        $user = $this->belongsTo('App\Models\User', 'seller');
        return $user->first();
    }
    public function product($col = 'name')
    {
        $user = $this->belongsTo('App\Models\Product', 'product');
        return $user->first();
    }
}
