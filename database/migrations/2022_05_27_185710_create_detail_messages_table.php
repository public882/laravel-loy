<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('message')->nullable();
            $table->string('channel')->nullable();
            $table->unsignedBigInteger('user')->nullable();
            $table->longText('content')->nullable();
            $table->boolean('is_read', ['0', '1'])->default(0);
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_messages');
    }
}
