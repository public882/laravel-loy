<?php

namespace App\Providers;

use App\Helpers\Helper;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Request;

class UrlPathServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('admin.layout.sidebar', function ($view) {
            $fullPagePath = Request::url();
            $admin_path= Helper::option('admin_path', 'admin');
            $envAdminCharCount = strlen($admin_path) + 1;
            $urlAfterRoot = substr($fullPagePath, strpos($fullPagePath, $admin_path) + $envAdminCharCount);
            $view->with('urlPath', $urlAfterRoot);
        });
    }
}
