@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('style')
<link href="{{asset('admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('AppDashboard') }}"> {{__('admin.dashboard') }}</a></li>
<li class="breadcrumb-item active">{{$page_title}}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('admin.settings.navigation',['tab'=>'authentication'])
        <div class="card mb-3">
            <form action="{{ route('settings.authentication') }}"  enctype="multipart/form-data" method="POST">
             @csrf
            <div class="card-body">
                {!!Helpers::displayAlert()!!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.default_user_role') }}</div>
                                <select style="width: 100%" class="shadow-none form-control select2 @error('default_user_role') is-invalid @enderror" id="default_user_role" name="default_user_role" required="">
                                    <option value="" disabled selected>{{ __('lang.select',['name' => __('lang.default_user_role')]) }}</option>
                                    @foreach (App\Models\Role::All() as $role)
                                        <option value="{{ $role->name }}" {{ Helpers::option('default_role')==$role->name ? "selected":"" }}>{{ Str::ucfirst($role->name) }}</option>
                                    @endforeach
                                </select>
                                @error('mail_driver')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class=" mb-3">{{ __('lang.allow_registration') }} :</div>
                                <div class="form-group m-checkbox-inline mb-0 custom-radio-ml row">
                                    <div class="col col-md-2 radio radio-success">
                                        <input id="allow_registration1" type="radio" name="allow_registration" value="1" data-original-title="" title="" {{ Helpers::option('register')=='1' ? "checked":"" }}>
                                        <label class="mb-0" for="allow_registration1">{{ __('lang.yes') }}</label>
                                    </div>
                                    <div class="col col-md-2 radio radio-success">
                                        <input id="allow_registration2" type="radio" name="allow_registration" value="0" data-original-title="" title="" {{ Helpers::option('register')=='0' ? "checked":"" }}>
                                        <label class="mb-0" for="allow_registration2">{{ __('lang.no') }}</label>
                                    </div>
                                </div>
                                @error('allow_registration')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class=" mb-3">{{ __('lang.verify_new_user_email') }} :</div>
                                <div class="form-group m-checkbox-inline mb-0 custom-radio-ml row">
                                    <div class="col col-md-2 radio radio-success">
                                        <input id="verify_new_user_email1" type="radio" name="verify_new_user_email" value="1" data-original-title="" title="" {{ Helpers::option('verify_email')=='1' ? "checked":"" }}>
                                        <label class="mb-0" for="verify_new_user_email1">{{ __('lang.yes') }}</label>
                                    </div>
                                    <div class="col col-md-2 radio radio-success">
                                        <input id="verify_new_user_email2" type="radio" name="verify_new_user_email" value="0" data-original-title="" title="" {{ Helpers::option('verify_email')=='0' ? "checked":"" }}>
                                        <label class="mb-0" for="verify_new_user_email2">{{ __('lang.no') }}</label>
                                    </div>
                                </div>
                                @error('verify_new_user_email')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class=" mb-3">{{ __('lang.auto_accept_user_registration') }} :</div>
                                <div class="form-group m-checkbox-inline mb-0 custom-radio-ml row">
                                    <div class="col col-md-2 radio radio-success">
                                        <input id="auto_accept_user_registration1" type="radio" name="auto_accept_user_registration" value="1" data-original-title="" title="" {{ Helpers::option('auto_accept_user_registration')=='1' ? "checked":"" }}>
                                        <label class="mb-0" for="auto_accept_user_registration1">{{ __('lang.yes') }}</label>
                                    </div>
                                    <div class="col col-md-2 radio radio-success">
                                        <input id="auto_accept_user_registration2" type="radio" name="auto_accept_user_registration" value="0" data-original-title="" title="" {{ Helpers::option('auto_accept_user_registration')=='0' ? "checked":"" }}>
                                        <label class="mb-0" for="auto_accept_user_registration2">{{ __('lang.no') }}</label>
                                    </div>
                                </div>
                                @error('auto_accept_user_registration')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>

                        </div>
                        @can('update','settings')
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary">{{ __('lang.update') }}</button>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
@section('script')
<script src="{{asset('admin/plugins/select2/js/select2.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.select2').select2();
    });
</script>
@endsection
