<?php
return[
    'asas'=>[
        'name'=>'sasa',
        'access'=>[
            'view'   => 'View Own',
            'viewAny'   => 'View All',
            'create' => 'Create',
            'update'   => 'Update',
            'delete' => 'Delete',
        ]
    ],

];
