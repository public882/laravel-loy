<?php

namespace App\Helpers;

use App\Library\Timezones;
use App\Models\Option;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class Helper{
    static function option($name, $default = false)
    {
        if (!Schema::hasTable('options')) {
            return false;
        }
        $data = Option::where('name', $name)->first();
        if (!$data) {
            return $default;
        }

        return $data->value;
    }

    static function defaultRole($default = 'user',$check_default=true){
        $default_role = Helper::option('default_role');
        if ($default_role && $check_default) {
            $default = $default_role;
        }
        if (!Schema::hasTable('roles')) {
            return false;
        }
        $role_default = Role::where('name', $default)->first();
        if ($role_default) {
            return $role_default;
        }
        $db = new Role();
        $db->name = $default;
        $db->save();
        return $db;
    }
    static function isAdmin(){
        $user = Auth::user();
        if (!$user) {
            abort(401);
        }
        if ($user->role == '1') {
            return true;
        }
        return false;
    }

    static function authorize($access,$module){
        $user= Auth::user();
        if(!$user){
            abort(401);
        }
        if (Gate::forUser($user)->denies($access, $module)) {
            abort(401);
        }
    }

    static function alert($message, $type = 'info')
    {
        Session::flash('message', $message);
        Session::flash('alert-class', $type);
    }

    static function displayAlert()
    {
        if (Session::has('message')) {
            $message = Session::get('message');
            $type = Session::get('alert-class');
            return sprintf('<div class="alert alert-%s">%s</div>', $type, $message);
        }

        return '';
    }


    static function user_access($access, $feature = null, $userid = 0)
    {
        $users = Permission::where('user_id', $userid)->where('module', $feature)->where('access', $access)->first();
        if (!$users) {
            return false;
        }
        return true;
    }

    static function has_permission($permission, $access = '', $userid = 0)
    {
        return Helper::user_access($access, $permission, $userid);
    }

    static function has_role_permission($role_id = 0, $access, $feature)
    {
        $roles = Role::find($role_id);
        if (!$roles) {
            return false;
        }
        $permissions = !empty($roles->access) ? unserialize($roles->access) : [];
        foreach ($permissions as $appliedFeature => $capabilities) {
            if ($feature == $appliedFeature && in_array($access, $capabilities)) {
                return true;
            }
        }
        return false;
    }
    static function action_links($link = array())
    {
        $act = [];
        foreach ((array)$link as $actions) {
            $title = 'link';
            $class = 'text-info';
            $link = '#';
            $onclick = '';
            if (array_key_exists("title", $actions)) {
                $title = $actions['title'];
            }
            if (array_key_exists("class", $actions)) {
                $class = $actions['class'];
            }
            if (array_key_exists("link", $actions)) {
                $link = $actions['link'];
            }
            if (array_key_exists("onclick", $actions)) {
                $link = 'javascript::';
                $onclick = 'onclick="' . $actions['onclick'] . '"';
            }
            $act[] = "<a class='" . $class . "' href='" . $link . "'" . $onclick . ">" . $title . "</a>";
        }
        $init     = implode(" | ", $act);
        return '<div class="row-option">' . $init . '</div>';
    }

    static function timezones_list()
    {
        return Timezones::get();
    }
}
