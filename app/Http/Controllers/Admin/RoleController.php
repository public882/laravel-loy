<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Helper::authorize('accessView','roles');
        $data['title']=__('admin.roles');
        $data['roles'] = Role::all();
        return view('admin.roles.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Helper::authorize('create', 'roles');
        $data['title'] = __('admin.new', ['name' => __('admin.roles')]);
        return view('admin.roles.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Helper::authorize('create', 'roles');
        $request->validate([
            'role_name' => ['required', 'max:255', 'unique:roles,name'],
        ]);
        $permiss = NULL;
        if ($request->permissions) {
            $permiss = serialize($request->permissions);
        }
        Role::create([
            'name' => $request->role_name,
            'access' => $permiss,
        ]);
        Helper::alert(__('alert.addDone'));
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Helper::authorize('update', 'roles');
        $roles = Role::find($id);
        if (!$roles || $id == '1') {
            Helper::alert(__('alert.not_found'), 'danger');
            return redirect()->route('roles.index');
        }
        $data['role'] = $roles;
        $data['title'] = __('admin.update') . ' ' . __('admin.roles') . ' : ' . $roles->name;
        return view('admin.roles.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        Helper::authorize('update', 'roles');
        $roles = Role::find($id);
        if (!$roles || $id == '1') {
            Helper::alert(__('alert.not_found'), 'danger');
            return redirect()->route('roles.index');
        }
        $request->validate([
            'role_name' => ['required', 'max:255', Rule::unique('roles', 'name')->ignore($roles->id, 'id')],
        ]);
        $roles->name = $request->role_name;
        $permiss = NULL;
        if ($request->permissions) {
            $permiss = serialize($request->permissions);
        }
        $roles->access = $permiss;
        $roles->save();

        if ($request->update_user_permissions == '1') {
            $roles = Role::find($id);
            $permissions = new UserController;
            $listUser = $roles->user()->get();
            foreach ($listUser as $user) {
                $permissions->update_permissions($request->permissions, $user->id);
            }
        }
        Helper::alert(__('alert.saveDone'));
        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Helper::authorize('delete', 'roles');
        $roles = Role::find($id);
        if (!$roles || $id == '1') {
            Helper::alert(__('admin.not_found'), 'danger');
            return redirect()->route('roles.index');
        }
        if ($roles->user()->count() > 0) {
            Helper::alert(__('admin.role_is_used',['number' => $roles->user()->count()]),'danger');
            return redirect()->route('roles.index');
        }
        Role::destroy($id);
        Helper::alert(__('alert.deleteDone'));
        return redirect()->route('roles.index');
    }
}
