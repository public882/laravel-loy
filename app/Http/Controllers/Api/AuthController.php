<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use ApiResponser;

    /**
     * Login
     *
     * @group Authentication
     * @bodyParam email string required Must be a valid email address.Example: jacksparrow@disney.com
     * @bodyParam password string required Must be at least 5 characters.
     * @response scenario=success{
     *  "message": "login success",
     *  "data": {
     *      "id": 1,
     *      "name": "jack",
     *      "email": "jacksparrow@site.com",
     *      "role": 2,
     *      "token": "112|fqnrDA1MIMDkYYr2UMOil3EpHFVYRT5fDsezYwdv"
     *  }
     * }
     * @response 404 scenario="user not found"{
     *  "message": "user not found",
     * }
     * @response 400 scenario="user email not verified"{
     *  "message": "user not verified",
     * }
     * @response 400 scenario="wrong password"{
     *  "message": "wrong password",
     * }
     */

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string|min:5'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $user = User::where('email', $request->email)->where('status', '1')->first();
        if (!$user) {
            return $this->failNotFound('user not found');
        }
        if ($user->email_verified_at ==null) {
            return $this->failValidationError('user not verified');
        }
        if (!Hash::check($request->password, $user->password)) {
            return $this->failValidationError('wrong password');
        }
        $token = $user->createToken('auth_token')->plainTextToken;
        $data=[
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role,
            'token'=> $token
        ];
        return $this->respond($data,'login success');
    }


    /**
     * Register
     *
     * @group Authentication
     * @bodyParam name string required name of user. Example: jack sparrow
     * @bodyParam email string required email of user and must valid email. Example: jacksparrow@disney.com
     * @bodyParam password string required password min: 5.
     * @bodyParam address string address of user. Example: Jl. mataram
     * @bodyParam address string address of user. Example: Jl. mataram
     * @bodyParam role int role id. Example: 2
     */
    public function register(Request $request){
        if( Helper::option('register', '0') == '0'){
            return $this->failNotFound();
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:5'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $fields = $request->all();
        $fields['password'] = Hash::make($fields['password']);
        $default_role = Helper::defaultRole();
        $role = $default_role ? $default_role->id : NULL;
        if(isset($request->role)){
            $roles=Role::findOrFail($request->role);
            $role = $roles->id;
        }
        var_dump($role);
        $fields['role'] = $role;
        $fields['email_verified_at'] = Helper::option('verify_email', '0') == '1' ? NULL : now();
        $fields['status'] = Helper::option('auto_accept_user_registration', '1') == '1' ? '1' : '0';
        var_dump($fields);
        $user = User::create($fields);
        $userController = new UserController;
        $permissions = unserialize($default_role->access);
        $userController->update_permissions($permissions, $user->id);
        return $this->respond($user, 'registration success');
    }

    /**
     * Get list roles
     *
     * @group Authentication
     */
    public function roles(){
        $data = Role::all();
        $res['message'] = 'data found';
        $res['total'] = count($data);
        $res['data'] = $data;
        return $this->successResponse($res);
    }


    /**
     * Logout
     *
     * @group Authentication
     * @authenticated
     */
    public function logout(Request $request){
      $request->user()->tokens()->delete();
      return $this->successResponse(['message'=>'logout success']);
    }

    /**
     * get current user
     *
     * @group User
     * @authenticated
     */

    public function user(Request $request){
        return $request->user();
    }


    /**
     * Update Profile
     *
     * @group User
     * @authenticated
     * @bodyParam name string required name of user. Example: jack sparrow
     * @bodyParam email string required email of user. Example: jacksparrow@disney.com
     * @bodyParam address string address of user. Example: Jl. mataram
     * @bodyParam phone string phone number of user. Example: 08218922121
     */

    public function updateProfile(Request $request){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $user = User::findOrFail($user->id);
        $fields=$request->all();
        $fields['role']= $user->role;
        $fields['status']= $user->status;
        $fields['email_verified_at']= $user->email_verified_at;
        $user->fill($fields);
        $user->save();
        $message = $message = "update profile success";
        return $this->respondUpdated($user, $message);
    }

    /**
     * Update Password
     *
     * @group User
     * @authenticated
     * @bodyParam password string required Must be at least 5 characters.
     * @bodyParam password_confirmation string required Must be at least 5 characters and equeal with password.

     */
    public function updatePassword(Request $request){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:5|confirmed',
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $user = User::findOrFail($user->id);
        $request->all();
        $fields['password'] = Hash::make($request->password);
        $user->fill($fields);
        $user->save();
        $message = $message = "update password success";
        return $this->respondUpdated($user, $message);
    }
}
