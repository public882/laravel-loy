<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory,Blameable;

    protected $fillable = [
        'transaction', 'product', 'name','description', 'price','image','qty','total','seller'
    ];


}
