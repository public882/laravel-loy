# Authenticating requests

Authenticate requests to this API's endpoints by sending an **`Authorization`** header with the value **`"Bearer {TOKEN}"`**.

All authenticated endpoints are marked with a `requires authentication` badge in the documentation below.

Silahkan <b><a href="#authentication-POSTapi-login" class="btn btn-primary">login</a></b> mengunakan email dan password kemudian gunakan <b>TOKEN</b> dari payload response yang ada untuk digunakan sebagai token authorization
