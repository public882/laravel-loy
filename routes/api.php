<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\DeliveryController;
use App\Http\Controllers\Api\FeedbackController;
use App\Http\Controllers\Api\MessageController;
use App\Http\Controllers\Api\PaymentController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\TransactionController;
use App\Http\Controllers\Api\VoucherController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/', function () {
    return response()
        ->json(['message'=>'page not found'],404); //default route
});

Route::post('/login', [AuthController::class, 'login'])->name('ApiLogin');//login
Route::post('/register', [AuthController::class, 'register'])->name('ApiRegister');//register
Route::get('/roles', [AuthController::class, 'roles'])->name('ApiGetRoles');//register

//route product

Route::get('/products', [ProductController::class, 'index'])->name('ApiGetProduct'); //get list product
Route::get('/products/{id}', [ProductController::class, 'show'])->name('ApiGetProductId'); //get product by id

Route::get('/delivery', [DeliveryController::class, 'index'])->name('ApiGetDelivery'); //get list delivery method
Route::get('/delivery/{id}', [DeliveryController::class, 'show'])->name('ApiGetDeliveryId'); //get delivery method by id

Route::get('/payment', [PaymentController::class, 'index'])->name('ApiGetPayment'); //get list payment method list
Route::get('/payment/{id}', [PaymentController::class, 'show'])->name('ApiGetPaymentId');//get payment method by id

Route::get('/voucher', [VoucherController::class, 'index'])->name('ApiGetVoucher'); //get list voucher
Route::get('/voucher/code/{id}', [VoucherController::class, 'showCode'])->name('ApiGetVoucherCode');//get voucher by voucher code
Route::get('/voucher/{id}', [VoucherController::class, 'show'])->name('ApiGetVoucherId');//get voucher by voucher id
Route::get('/product/feedback/{id}', [FeedbackController::class, 'productFeedback'])->name('ApiGetProductFeedback');//get feedback by product id
Route::get('/seller/feedback/{id}', [FeedbackController::class, 'sellerFeedback'])->name('ApiGetSellerFeedback');//get feedback by seller id

Route::Group(
    [
        'middleware' => ['auth:sanctum']
    ],
    function(){
        Route::get('/user', [AuthController::class, 'user'])->name('ApiUserLogin');//logout
        Route::post('/logout', [AuthController::class, 'logout'])->name('ApiLogout');//logout
        Route::post('/profile', [AuthController::class, 'updateProfile'])->name('ApiUpdateProfile');//update profile
        Route::post('/password', [AuthController::class, 'updatePassword'])->name('ApiUpdatePassword');//update password
        Route::post('/products', [ProductController::class, 'store'])->name('ApiCreateProduct'); //create product
        Route::put('/products/{id}', [ProductController::class, 'update'])->name('ApiPutProduct'); //edit product
        Route::patch('/products/{id}', [ProductController::class, 'update'])->name('ApiPatchProduct');//edit product
        Route::delete('/products/{id}', [ProductController::class, 'delete'])->name('ApiDeleteProduct');//delete product
        //deliver method
        Route::post('/delivery', [DeliveryController::class, 'store'])->name('ApiCreateDelivery');//create delivery method (admin only)
        Route::put('/delivery/{id}', [DeliveryController::class, 'update'])->name('ApiPutDelivery');//update delivery method (admin only)
        Route::patch('/delivery/{id}', [DeliveryController::class, 'update'])->name('ApiPatchDelivery');//update delivery method (admin only)
        Route::delete('/delivery/{id}', [DeliveryController::class, 'delete'])->name('ApiDeleteDelivery');//delete delivery method (admin only)
        // payment method
        Route::post('/payment', [PaymentController::class, 'store'])->name('ApiCreatePayment');//create payment method (admin only)
        Route::put('/payment/{id}', [PaymentController::class, 'update'])->name('ApiPutPayment');//update payment method (admin only)
        Route::patch('/payment/{id}', [PaymentController::class, 'update'])->name('ApiPatchPayment');//update payment method (admin only)
        Route::delete('/payment/{id}', [PaymentController::class, 'delete'])->name('ApiDeletePayment');//delete payment method (admin only)

        //voucher
        Route::post('/voucher', [VoucherController::class, 'store'])->name('ApiCreateVoucher');//create voucher code (admin only)
        Route::put('/voucher/{id}', [VoucherController::class, 'update'])->name('ApiPutVoucher');//update voucher code (admin only)
        Route::patch('/voucher/{id}', [VoucherController::class, 'update'])->name('ApiPatchVoucher');//update voucher code (admin only)
        Route::delete('/voucher/{id}', [VoucherController::class, 'delete'])->name('ApiDeleteVoucher');//delete voucher code (admin only)

        //cart
        Route::get('/cart', [TransactionController::class, 'cart'])->name('ApiGetCart');//get list cart (current user)
        Route::post('/cart', [TransactionController::class, 'addCart'])->name('ApiAddCart');//add cart (current user)
        Route::delete('/cart/{id}', [TransactionController::class, 'deleteCart'])->name('ApiDeleteCart');//delete product cart (current user)

        //transaction
        Route::post('/checkout', [TransactionController::class, 'checkout'])->name('ApiCheckout');//checkout cart (current user)
        Route::get('/purchase', [TransactionController::class, 'purchase'])->name('ApiGetPurchase'); //get list purchase (current user / buyer)
        Route::get('/order', [TransactionController::class, 'order'])->name('ApiGetOrder');//get list order (current user / seller)
        Route::get('/admin-transactions', [TransactionController::class, 'adminTransactionList'])->name('ApiAdminGetTransactions');//get list transaction for admin

        //process transaction
        Route::post('/process/{id}', [TransactionController::class, 'confirmOrder'])->name('ApiConfirmOrder'); //for seller process
        Route::post('/send/{id}', [TransactionController::class, 'sellerSend'])->name('ApiSendOrder'); //for seller send
        Route::post('/cancel/seller/{id}', [TransactionController::class, 'sellerCancel'])->name('ApiSellerCancel'); //for seller cancel
        Route::post('/cancel/buyer/{id}', [TransactionController::class, 'buyerCancel'])->name('ApiBuyerCancel'); //for buyer cancel
        Route::post('/done/{id}', [TransactionController::class, 'updateDelivery'])->name('ApiBuyerDone'); //for buyer done
        Route::post('/confirm-admin-done/{id}', [TransactionController::class, 'adminConfirmDelivery'])->name('ApiAdminDone'); //for admin done

        //feedback
        Route::get('/transaction/feedback/{id}', [FeedbackController::class, 'transactionFeedback'])->name('ApiGetTransactionFeedback');//get feedback by transaction id
        Route::post('/transaction/feedback', [FeedbackController::class, 'postFeedback'])->name('ApiPostTransactionFeedback');//post feedback by transaction id & product id

        //chat/message
        Route::get('/messages', [MessageController::class, 'messages'])->name('ApiGetMessage');//get list message for currrent user
        Route::get('/messages/{id}', [MessageController::class, 'detailMessage'])->name('ApiGetMessageDetail');//get Detail list message by message channel
        Route::post('/messages', [MessageController::class, 'postMessage'])->name('ApipostMessage');//post message
    }
);

