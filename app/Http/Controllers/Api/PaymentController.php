<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    use ApiResponser;

    /**
     * Get list payment method
     *
     * @group Payment method
     * @queryParam orderby string order record by column name (default: name). Example:name
     * @queryParam sort string sort record [ASC,DESC] (default: DESC). Example:DESC
     * @queryParam status string show payment for active status[all=>all,1=>active,0=>inactive] (default: 1). Example:1
     * @queryParam search string search by payment name or payment description. Example:

     */
    public function index(Request $request)
    {
        $orderby = $request->has('orderby') ? $request->get('orderby') : 'name';
        $sort = $request->has('sort') ? $request->get('sort') : 'ASC';
        $status = $request->has('status') ? $request->get('status') : '1';

        $payment = Payment::query();
        if ($request->has('search')) {
            $search = $request->get('search');
            $column = ['name', 'description'];
            foreach ($column as $key => $col) {
                if ($key == 0) {
                    $payment->where($col, 'like', '%' . $search . '%');
                } else {
                    $payment->orWhere($col, 'like', '%' . $search . '%');
                }
            }
        }
        if ($status != 'all') {
            $payment->where('status', $status);
        }
        $payment->orderBy($orderby, $sort);
        $result = $payment->get();
        $total = $payment->count();
        $data = $result->map(function ($item) {
            return $item;
        });
        $res['message'] = 'data found';
        $res['total'] = $total;
        $res['data'] = $data;
        return $this->successResponse($res);
    }

    /**
     * Get payment method by id
     *
     * @group Payment method
     */

    public function show(Request $request, $id)
    {
        $status = $request->has('status') ? $request->get('status') : '1';
        $payment = Payment::findOrFail($id);
        if ($status != 'all' && $payment->status != 1) {
            return $this->failNotFound("data not found");
        }
        return $this->respond($payment);
    }

    /**
     * Create payment method
     *
     * <aside class="notice">only admin user can create payment method.</aside>
     * @group Payment method
     * @authenticated
     * @bodyParam description string required data description. Example: Transfer ke BNI no rekening admin 9023232323
     * @bodyParam status boolean status active or inactive. true=>active, false=>inactive. Example:1
     */

    public function store(Request $request)
    {
        if (!Helper::isAdmin()) {
            return $this->failUnauthorized();
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'required',
            'status' => 'boolean'

        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $fields = $request->all();
        $fields['name'] = $fields['name'];
        $fields['description'] = $fields['description'];
        $payment = Payment::create($fields);
        $message = 'create success';
        $id = $payment->id;
        $paymentData = Payment::findOrFail($id);
        return $this->respondCreated($paymentData, $message);
    }

    /**
     * Update payment method by id
     *
     * <aside class="notice">only admin user can update payment method.</aside>
     * @group Payment method
     * @bodyParam description string required data description. Example: Transfer ke BNI no rekening admin 9023232323
     * @bodyParam status boolean status active or inactive. true=>active, false=>inactive. Example:1
     * @authenticated
     */

    public function update(Request $request, $id)
    {
        $payment = Payment::findOrFail($id);
        if (!Helper::isAdmin()) {
            return $this->failUnauthorized();
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'required',
            'status' => 'boolean'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $payment->fill($request->all());
        $payment->save();
        $message =  "update success";
        $productData = Payment::findOrFail($id);
        return $this->respondUpdated($productData, $message);
    }


    /**
     * Delete payment method by id
     *
     * <aside class="notice">only admin user can delete payment method.</aside>
     * @group Payment method
     * @authenticated
     */

    public function delete($id)
    {
        $delivery = Payment::findOrFail($id);
        if (!Helper::isAdmin()) {
            return $this->failUnauthorized();
        }
        $delivery->delete();
        $message = "delete success";
        return $this->respondDeleted($id, $message);
    }
}
