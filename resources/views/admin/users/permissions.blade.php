<?php
$permissions=config('permissions');
?>
<div class="table-responsive mb-3">
    <table class="table table-bordered roles">
        <thead>
            <tr>
                <th scope="col">{{ __('admin.feature') }}</th>
                <th scope="col">{{ __('admin.access') }}</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($permissions  as $feature => $permission)
            <tr data-name="{{ $feature }}">
                <th scope="row">{{ $permission['name'] }}</th>
                <td >
                    <div class="form-group m-b-0 ">
                        @foreach ($permission['access'] as $access => $name)
                        <?php
                        $checked = '';
                        $disabled = '';
                        if(( isset($user) && $user->roles()=='admin')||
                                (is_array($name) && isset($name['not_applicable']) && $name['not_applicable'])
                        ){
                        $disabled = ' disabled ';
                        }elseif (
                            (isset($user) && Helpers::user_access($access, $feature, $user->id))
                            || isset($role) && Helpers::has_role_permission($role->id, $access, $feature))
                        {
                        $checked = ' checked ';
                        }
                        ?>

                        <div class="checkbox checkbox-success">
                            <input id="{{ $feature.'_'.$access }}"
                            type="checkbox" name="permissions[{{ $feature }}][]"
                            value="{{ $access }}" {{ $checked }} {{ $disabled }}
                            {{ $access=='viewAny' ? 'data-can-view':'' }}
                            {{ $access=='view' ? 'data-can-view-own':'' }}
                            <?php if(is_array($name) && isset($name['not_applicable']) && $name['not_applicable']){ ?> data-not-applicable="true" <?php } ?>
                            >
                            <label for="{{ $feature.'_'.$access }}">{{ !is_array($name) ? $name : $name['name'] }}</label>
                        </div>
                        @endforeach
                    </div>
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>
