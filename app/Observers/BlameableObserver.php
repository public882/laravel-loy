<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BlameableObserver
{
    public function creating(Model $model)
    {

        $model->created_by = $this->auth_user();
        $model->updated_by = $this->auth_user();
    }

    public function updating(Model $model)
    {
        $model->updated_by = $this->auth_user();
    }

    private function auth_user(){
        $user_id=NULL;
        $user=Auth::user();
        if($user){
            $user_id=$user->id;
        }
        return $user_id;
    }
}
