@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('breadcrumb')
<li class="breadcrumb-item active">{{ __('admin.dashboard') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        CONTENT HERE
    </div>
</div>
<!-- end row -->
@endsection
