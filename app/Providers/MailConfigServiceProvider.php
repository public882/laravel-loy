<?php

namespace App\Providers;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $config = array(
            'driver'     => $this->get_option('mail_driver','smtp'),
            'host'       => $this->get_option('mail_host', 'smtp.mailtrap.io'),
            'port'       => $this->get_option('mail_port', '2525'),
            'from'       => array(
                'address' => $this->get_option('mail_from_address', 'example@mail.com'),
                'name' => $this->get_option('mail_from_name', 'example@mail.com'),
            ),
            'encryption' => $this->get_option('mail_encryption', 'tls'),
            'username'   => $this->get_option('mail_username'),
            'password'   => $this->get_option('mail_password'),
            'sendmail'   => '/usr/sbin/sendmail -bs',
            'pretend'    => false,
        );
        Config::set('mail', $config);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function get_option($option,$default=''){
        if (Schema::hasTable('options')) {
            $data = DB::table('options')->where('name', '=', $option)->first();
            if($data){
                return $data->value;
            }else{
                return $default;
            }
        }else{
            return $default;
        }
    }
}
