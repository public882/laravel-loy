<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Rating;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FeedbackController extends Controller
{
    use ApiResponser;

    /**
     * Create Feedback
     *
     * @group Feedback / Rating
     * @authenticated
     * @bodyParam comment string.
     * @bodyParam transaction int required Transaction ID.
     * @bodyParam product int required Product ID.
     */

    public function postFeedback(Request $request){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $validator = Validator::make($request->all(), [
            'transaction' => 'required',
            'product' => 'required',
            'rating' => 'required|numeric|min:1|max:5',
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $fields=$request->all();
        $transactionDetail=TransactionDetail::where('transaction', $request->transaction)->where('product', $request->product)->first();
        $transaction=Transaction::where('id', $request->transaction)->where('user', $user->id)->first();
        if(!$transactionDetail){
            return $this->failNotFound('transaction not found');
        }
        if(!$transaction){
            return $this->failNotFound('transaction not found');
        }
        $fields['user']= $transaction->user;
        $fields['seller']= $transaction->seller;
        $existsRating=Rating::where('transaction', $request->transaction)->where('product', $request->product)->where('user', $user->id)->first();
        if(!$existsRating){
            $feedback = Rating::create($fields);
        }else{
            $existsRating->fill($fields);
            $existsRating->save();
            $feedback =$existsRating;
        }
        $message = 'post feedback success';
        return $this->respondCreated($feedback, $message);

    }

    /**
     * Get Feeback by transaction id
     *
     * @group Feedback / Rating
     * @authenticated
     */

    public function transactionFeedback($id){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }

        $ratings=Rating::where('transaction',$id)->get();

        foreach($ratings as $key => $trans){
            $trans['transaction']=$trans->trans();
            $trans['product']=$trans->product();
            $trans['seller']=$trans->seller();
            $trans['user']=$trans->user();
        }
        $ress['message'] = 'data found';
        $ress['total'] = count($ratings);
        $ress['data'] = $ratings;
        return $this->successResponse($ress);
    }

    /**
     * Get Feeback by product id
     *
     * @group Feedback / Rating
     */

    public function productFeedback($id){
        $ratings=Rating::where('product',$id)->get();
        $rating=0;
        foreach($ratings as $key => $trans){
            $trans['transaction']=$trans->trans();
            $trans['product']=$trans->product();
            $trans['seller']=$trans->seller();
            $trans['user']=$trans->user();
            $rating= $trans['rating']+$rating;
        }
        $total = count($ratings);
        $ress['message'] = 'data found';
        $ress['total'] = $total;
        $ress['rating'] = $total > 0 ? round((int)$rating / (int)count($ratings),2) : 0;
        $ress['data'] = $ratings;
        return $this->successResponse($ress);
    }

    /**
     * Get Feeback by user / seller id
     *
     * @group Feedback / Rating
     */

    public function sellerFeedback($id){
        $ratings=Rating::where('seller',$id)->get();
        $rating=0;
        foreach($ratings as $key => $trans){
            $trans['transaction']=$trans->trans();
            $trans['product']=$trans->product();
            $trans['seller']=$trans->seller();
            $trans['user']=$trans->user();
            $rating= $trans['rating']+$rating;
        }
        $total= count($ratings);
        $ress['message'] = 'data found';
        $ress['total'] = $total;
        $ress['rating'] = $total>0 ? round((int)$rating/ (int)count($ratings),2):0;
        $ress['data'] = $ratings;
        return $this->successResponse($ress);
    }
}
