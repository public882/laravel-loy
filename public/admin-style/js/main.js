$("[data-can-view-own], [data-can-view]").on("change", function() {
    var is_own_attr = $(this).attr("data-can-view-own");
    view_chk_selector = $(this)
        .parents("tr")
        .find(
            "td input[" +
                (typeof is_own_attr !== typeof undefined &&
                is_own_attr !== false
                    ? "data-can-view"
                    : "data-can-view-own") +
                "]"
        );

    if (view_chk_selector.data("not-applicable") == true) {
        return;
    }

    view_chk_selector.prop("checked", false);
    view_chk_selector.prop("disabled", $(this).prop("checked") === true);
});

function change_roles_permissions(url, roleid, user_changed) {
    roleid =
        typeof roleid == "undefined" ? $('select[name="role"]').val() : roleid;
    var isedit = $('.member > input[name="isedit"]');
    // Check if user is edit view and user has changed the dropdown permission if not only return
    if (
        isedit.length > 0 &&
        typeof roleid !== "undefined" &&
        typeof user_changed == "undefined"
    ) {
        return;
    }

    // Administrators does not have permissions
    if ($('input[name="administrator"]').prop("checked") === true) {
        return;
    }

    // Last if the roleid is blank return
    if (roleid === "") {
        return;
    }

    // Get all permissions
    var permissions = $("table.roles").find("tr");
    requestGetJSON(url).done(function(response) {
        permissions
            .find(".capability")
            .not('[data-not-applicable="true"]')
            .prop("checked", false)
            .trigger("change");

        $.each(permissions, function() {
            var row = $(this);
            $.each(response, function(feature, obj) {
                if (row.data("name") == feature) {
                    $.each(obj, function(i, capability) {
                        row.find(
                            'input[id="' + feature + "_" + capability + '"]'
                        ).prop("checked", true);
                        if (capability == "viewAny") {
                            row.find("[data-can-view]").change();
                        } else if (capability == "view") {
                            row.find("[data-can-view-own]").change();
                        }
                    });
                }
            });
        });
    });
}

function requestGetJSON(uri, params) {
    params = typeof params == "undefined" ? {} : params;
    params.dataType = "json";
    return requestGet(uri, params);
}
function requestGet(uri, params) {
    params = typeof params == "undefined" ? {} : params;
    var options = {
        type: "GET",
        url: uri
    };
    return $.ajax($.extend({}, options, params));
}

//upload product image update page
$(document).on("change", "#Multifileupload", function() {
    var MultifileUpload = document.getElementById("Multifileupload");
    if (typeof FileReader != "undefined") {
        var MultidvPreview = document.getElementById("MultidvPreview");
        MultidvPreview.innerHTML = "";
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        for (var i = 0; i < MultifileUpload.files.length; i++) {
            var file = MultifileUpload.files[i];
            var reader = new FileReader();
            reader.onload = function(e) {
                var img = document.createElement("IMG");
                img.height = "100";
                img.src = e.target.result;
                img.id = "Multifileupload_image";
                MultidvPreview.appendChild(img);
                $("#Multifileupload_button").show();
            };
            reader.readAsDataURL(file);
        }
    } else {
        alert("This browser does not support HTML5 FileReader.");
    }
});
