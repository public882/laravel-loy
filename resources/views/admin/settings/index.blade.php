@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('AppDashboard') }}"> {{__('admin.dashboard') }}</a></li>
<li class="breadcrumb-item active">{{$page_title}}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('admin.settings.navigation',['tab'=>'general'])
        <div class="card mb-3">
            <form action="{{ route('settings.general') }}"  enctype="multipart/form-data" method="POST">
             @csrf
            <div class="card-body">
                {!!Helpers::displayAlert()!!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.site_title') }}</div>
                                <input class="shadow-none form-control @error('site_title') is-invalid @enderror" type="text" id="site_title" value="{{ old('site_title') ?? Helpers::option('site_title')  }}" name="site_title" placeholder="{{ __('lang.site_title') }}">
                                @error('site_title')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.site_description') }}</div>
                                <textarea class="shadow-none form-control" id="site_description" name="site_description" rows="3">{{ old('site_description') ?? Helpers::option('site_description')  }}</textarea>
                                @error('site_description')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.site_keyword') }}</div>
                                <input class="shadow-none form-control @error('site_keyword') is-invalid @enderror" type="text" id="site_keyword" value="{{ old('site_keyword') ?? Helpers::option('site_keyword')  }}" name="site_keyword" placeholder="{{ __('lang.site_keyword') }}">
                                @error('site_keyword')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.site_url') }}</div>
                                <input class="shadow-none form-control @error('site_url') is-invalid @enderror" type="text" id="site_url" value="{{ old('site_url') ?? Helpers::option('site_url')  }}" name="site_url" placeholder="{{ __('lang.site_url') }}">
                                @error('site_url')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>

                            <div class="col-md-12 mb-3">
                             @can('update','settings')
                            <div class="form-footer">
                                <button type="submit" class="btn btn-primary">{{ __('lang.update') }}</button>
                            </div>
                            @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
