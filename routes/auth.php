<?php

use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;


Auth::routes([
    'verify' => Helper::option('verify_email', '0') === '1' ? true : false,
    'register' => Helper::option('register') === '1' ? true : false
]);

