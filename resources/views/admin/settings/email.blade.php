@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('AppDashboard') }}"> {{__('admin.dashboard') }}</a></li>
<li class="breadcrumb-item active">{{$page_title}}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('admin.settings.navigation',['tab'=>'email'])
        <div class="card mb-3">
            <form action="{{ route('settings.email') }}"  enctype="multipart/form-data" method="POST">
             @csrf
            <div class="card-body">
                {!!Helpers::displayAlert()!!}
                <div class="row">
                    <div class="row">
                        <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.mail_driver') }}</div>
                                <input class="shadow-none form-control @error('mail_driver') is-invalid @enderror" type="text" id="mail_driver" value="{{ old('mail_driver') ?? Helpers::option('mail_driver')  }}" name="mail_driver" placeholder="{{ __('lang.mail_driver') }}">
                                @error('mail_driver')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.mail_host') }}</div>
                                <input class="shadow-none form-control @error('mail_host') is-invalid @enderror" type="text" id="mail_host" value="{{ old('mail_host') ?? Helpers::option('mail_host')  }}" name="mail_host" placeholder="{{ __('lang.mail_host') }}">
                                @error('mail_host')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.mail_port') }}</div>
                                <input class="shadow-none form-control @error('mail_port') is-invalid @enderror" type="text" id="mail_port" value="{{ old('mail_port') ?? Helpers::option('mail_port')  }}" name="mail_port" placeholder="{{ __('lang.mail_port') }}">
                                @error('mail_port')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.mail_username') }}</div>
                                <input class="shadow-none form-control @error('mail_username') is-invalid @enderror" type="text" id="mail_username" value="{{ old('mail_username') ?? Helpers::option('mail_username')  }}" name="mail_username" placeholder="{{ __('lang.mail_username') }}">
                                @error('mail_username')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.mail_password') }}</div>
                                <input class="shadow-none form-control @error('mail_password') is-invalid @enderror" type="text" id="mail_password" value="{{ old('mail_password') ?? Helpers::option('mail_password')  }}" name="mail_password" placeholder="{{ __('lang.mail_password') }}">
                                @error('mail_password')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.mail_encryption') }}</div>
                                <input class="shadow-none form-control @error('mail_encryption') is-invalid @enderror" type="text" id="mail_encryption" value="{{ old('mail_encryption') ?? Helpers::option('mail_encryption')  }}" name="mail_encryption" placeholder="{{ __('lang.mail_encryption') }}">
                                @error('mail_encryption')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.mail_sender') }}</div>
                                <input class="shadow-none form-control @error('mail_sender') is-invalid @enderror" type="email" id="mail_sender" value="{{ old('mail_sender') ?? Helpers::option('mail_from_address')  }}" name="mail_sender" placeholder="{{ __('lang.mail_sender') }}">
                                @error('mail_sender')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="col-form-label">{{ __('lang.mail_sender_name') }}</div>
                                <input class="shadow-none form-control @error('mail_sender_name') is-invalid @enderror" type="text" id="mail_sender_name" value="{{ old('mail_sender_name') ?? Helpers::option('mail_from_name')  }}" name="mail_sender_name" placeholder="{{ __('lang.mail_sender_name') }}">
                                @error('mail_sender_name')
                                    <div class="invalid-feedback" >
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                        </div>
                        @can('update','settings')
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary">{{ __('lang.update') }}</button>
                        </div>
                        @endcan
                        </div>

                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
