<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory,Blameable,SoftDeletes;

    protected $fillable = [
        'name', 'access'
    ];

    public function user()
    {
        return $this->hasMany('App\Models\User', 'role');
    }
}
