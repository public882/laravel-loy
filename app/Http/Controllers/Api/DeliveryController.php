<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Delivery;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DeliveryController extends Controller
{
    use ApiResponser;

    /**
     * Get list delivery method
     *
     * @group Delivery method
     * @queryParam orderby string order record by column name (default: name). Example:name
     * @queryParam sort string sort record [ASC,DESC] (default: DESC). Example:DESC
     * @queryParam status string show delivery for active status[all=>all,1=>active,0=>inactive] (default: 1). Example:1
     * @queryParam search string search by delivery name or delivery description. Example:

     */

    public function index(Request $request)
    {
        $orderby = $request->has('orderby') ? $request->get('orderby') : 'name';
        $sort = $request->has('sort') ? $request->get('sort') : 'ASC';
        $status = $request->has('status') ? $request->get('status') : '1';

        $delivery = Delivery::query();
        if ($request->has('search')) {
            $search = $request->get('search');
            $column = ['name', 'description'];
            foreach ($column as $key => $col) {
                if ($key == 0) {
                    $delivery->where($col, 'like', '%' . $search . '%');
                } else {
                    $delivery->orWhere($col, 'like', '%' . $search . '%');
                }
            }
        }
        if ($status != 'all') {
            $delivery->where('status', $status);
        }
        $delivery->orderBy($orderby, $sort);
        $result = $delivery->get();
        $total = $delivery->count();
        $data = $result->map(function ($item) {
            return $item;
        });
        $res['message'] = 'data found';
        $res['total'] = $total;
        $res['data'] = $data;
        return $this->successResponse($res);
    }

    /**
     * Get delivery method by id
     *
     * @group Delivery method
     */

    public function show(Request $request, $id)
    {
        $status = $request->has('status') ? $request->get('status') : '1';
        $payment = Delivery::findOrFail($id);
        if ($status != 'all' && $payment->status != 1) {
            return $this->failNotFound("data not found");
        }
        return $this->respond($payment);
    }


    /**
     * Create delivery method
     *
     * <aside class="notice">only admin user can create delivery method.</aside>
     * @group Delivery method
     * @authenticated
     * @bodyParam description string data description. Example: Pengiriman melalui JNE
     * @bodyParam status boolean status active or inactive. true=>active, false=>inactive. Example:1
     */

    public function store(Request $request)
    {
        if (!Helper::isAdmin()) {
            return $this->failUnauthorized();
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'price' => 'required|numeric|min:0',
            'status' => 'boolean'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $fields = $request->all();
        $fields['name'] = $fields['name'];
        if (isset($fields['description'])) {
            $fields['description'] = $fields['description'];
        }
        $fields['price'] = $fields['price'];
        $delivery = Delivery::create($fields);
        $message = 'create success';
        $id = $delivery->id;
        $deliveryData = Delivery::findOrFail($id);
        return $this->respondCreated($deliveryData, $message);
    }

    /**
     * Update delivery method by id
     *
     * <aside class="notice">only admin user can update delivery method.</aside>
     * @group Delivery method
     * @authenticated
     * @bodyParam description string data description. Example: Pengiriman melalui JNE
     * @bodyParam status boolean status active or inactive. true=>active, false=>inactive. Example:1
     */

    public function update(Request $request, $id)
    {
        $delivery = Delivery::findOrFail($id);
        if (!Helper::isAdmin()) {
            return $this->failUnauthorized();
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'price' => 'required|numeric|min:0',
            'status' => 'boolean'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $delivery->fill($request->all());
        $delivery->save();
        $message =  "update success";
        $productData = Delivery::findOrFail($id);
        return $this->respondUpdated($productData, $message);
    }

    /**
     * Delete delivery method by id
     *
     * <aside class="notice">only admin user can delete delivery method.</aside>
     * @group Delivery method
     * @authenticated
     */

    public function delete($id)
    {
        $delivery = Delivery::findOrFail($id);
        if (!Helper::isAdmin()) {
            return $this->failUnauthorized();
        }
        $delivery->delete();
        $message = "delete success";
        return $this->respondDeleted($id, $message);
    }
}
