<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as AccessGate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(AccessGate $gate)
    {
        $this->registerPolicies();

        $gate->before(function ($user) {
            if ($user->roles() == 'admin') {
                return true;
            }
        });

        $gate->define('viewAny', function ($user, $module) {
            return $user->permissions()->where(['access' => 'viewAny', 'module' => $module])->first();
        });
        $gate->define('view', function ($user, $module) {
            return $user->permissions()->where(['access' => 'view', 'module' => $module])->first();
        });
        $gate->define('create', function ($user, $module) {
            return $user->permissions()->where(['access' => 'create', 'module' => $module])->first();
        });
        $gate->define('update', function ($user, $module) {
            return $user->permissions()->where(['access' => 'update', 'module' => $module])->first();
        });
        $gate->define('delete', function ($user, $module) {
            return $user->permissions()->where(['access' => 'delete', 'module' => $module])->first();
        });
        $gate->define('restore', function ($user, $module) {
            return $user->permissions()->where(['access' => 'restore', 'module' => $module])->first();
        });
        $gate->define('forceDelete', function ($user, $module) {
            return $user->permissions()->where(['access' => 'forceDelete', 'module' => $module])->first();
        });
        $gate->define('accessView', function ($user, $module) {
            $viewAny = $user->permissions()->where(['access' => 'viewAny', 'module' => $module])->first();
            if ($viewAny) {
                return true;
            }
            $viewAny = $user->permissions()->where(['access' => 'view', 'module' => $module])->first();
            if ($viewAny) {
                return true;
            }
            return false;
        });
    }
}
