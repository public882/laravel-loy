<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Delivery;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\Voucher;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    use ApiResponser;

    /**
     * Get Cart
     *
     * @group Transaction
     * @authenticated
     */

    public function cart(){
        $user=Auth::user();
        if(!$user){
            return $this->failNotFound();
        }

        $cart = Cart::query();
        $cart->where('user', $user->id);
        $result = $cart->get();

        foreach ($result as $key => $res) {
            $product = $res->product();
            if ($product) {
                $res['product'] = $product;
                $res['user'] = $res->user();
                $res['total'] = $res->qty * $product->price;
                if($res['product']->status!=1){
                    unset($result[$key]);
                }
            }else{
                unset($result[$key]);
            }


        }
        $total = $result->count();
        $ress['message'] = 'data found';
        $ress['total'] = $total;
        $ress['data'] = $result;
        return $this->successResponse($ress);
    }

    /**
     * Add product to Cart
     *
     * if product already exists in cart, qty will added to current product in cart
     * @group Transaction
     * @authenticated
     * @bodyParam product int required Product ID.
     */

    public function addCart(Request $request){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'qty' => 'numeric',
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $product=Product::findOrFail($request->product);
        if($product->status!=1){
            return $this->failNotFound();
        }
        $fields = $request->all();
        $qty=1;
        if(isset($request->qty)){
            $qty= $request->qty;
        }
        $fields['qty']=$qty;
        $fields['user']= $user->id;
        $fields['seller']= $product->created_by;
        $cart=Cart::where('product', $request->product)->where('user', $user->id)->first();
        if($cart){
            $fields['qty']=$qty + $cart->qty;
            if($product->qty - $fields['qty']<0){
                return $this->failValidationError('qty product ' . $product->name . ' is ' . $product->qty . ' but you want ' . $fields['qty']);
            }
            $cart->fill($fields);
            $cart->save();
        }else{
            if ($product->qty - $fields['qty'] < 0) {
                return $this->failValidationError('qty product ' . $product->name . ' is ' . $product->qty . ' but you want ' . $fields['qty']);
            }
            $cart=Cart::create($fields);
        }
        $cart['product'] = $product;
        $cart['user'] = $user;
        $cart['total'] = $cart->qty * $product->price;
        $message='add cart success';
        return $this->respondCreated($cart, $message);
    }

    /**
     * Delete product from Cart
     *
     * delete 1 product by id from cart
     * @group Transaction
     * @authenticated
     * @urlParam product int required Product ID.
     */

    public function deleteCart($id){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $cart=Cart::findOrFail($id);
        if($cart->user != $user->id){
            return $this->failNotFound();
        }
        $cart->delete();
        $message = "delete cart success";
        return $this->respondCreated( $message);
    }

    /**
     * CHECKOUT
     *
     * only 1 seller for 1 checkout can be accept. checkout from cart.
     * qty from product will be reduced when checkout.
     * @group Transaction
     * @authenticated
     * @bodyParam payment int required Payment Method ID.
     * @bodyParam delivery int required Delivery Method ID.
     * @bodyParam address string required billing address.
     * @bodyParam voucher string voucher code. Example:FREE
     * @bodyParam notes string notes for seller. Example:Perhatikan packagingnya
     */

    public function checkout(Request $request){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $validator = Validator::make($request->all(), [
            'payment' => 'required',
            'delivery' => 'required',
            'address' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $fields = $request->all();
        $fields['voucher']=null;
        $fields['code']=null;
        $fields['discount']=0;
        if (isset($request->voucher)) {
            $voucher=Voucher::where('code', $request->voucher)->first();
            if($voucher){
                $fields['voucher'] = $voucher->id;
                $fields['code'] = $voucher->code;
                $fields['discount'] = $voucher->discount;
            }else{
                return $this->failValidationError('voucher code not found');
            }
        }
        $payment=Payment::where('id', $request->payment)->where('status','1')->first();
        if (!$payment) {
            return $this->failValidationError('payment method not found');
        }
        $delivery=Delivery::where('id', $request->delivery)->where('status','1')->first();
        if (!$delivery) {
            return $this->failValidationError('delivery method not found');
        }
        $fields['delivery'] = $delivery->id;
        $fields['delivery_fee'] = $delivery->price;
        $total = 0;
        $cartData=Cart::where('user', $user->id)->get();
        $currSeller=null;
        $countItem= count($cartData);
        if($countItem>0){
            foreach ($cartData as $key => $prod) {
                $total_prod = 0;
                if(!isset($prod['qty']) || $prod['qty']<1){
                    return $this->failNotFound('product qty not set');
                }
                if ($currSeller != null) {
                    if ($currSeller !== $prod['seller']) {
                        return $this->failValidationError('only 1 seller allowed for 1 checkout');
                    }
                }

                $product=Product::where('id',$prod['product'])->where('status','1')->first();
                if(!$product){
                    $countItem= $countItem-1;
                }else{
                    if ($product->qty - $prod['qty'] < 0) {
                        return $this->failValidationError('qty product '. $product->name.' is '. $product->qty.' but your cart is '. $prod['qty'].', please update your cart');
                    }
                    $total_prod = $product->price * $prod['qty'];
                    $total = $total + $total_prod;
                    $currSeller = $prod['seller'];
                }


            }
        }else{
            return $this->failValidationError('cart is empty');
        }
        if ($countItem<1) {
            return $this->failValidationError('cart is empty');
        }
        $date=Carbon::parse(now())->format('Ymd');
        $fields['seller'] = $currSeller;
        $fields['total'] = $total;
        $total_pay= ($total + $fields['delivery_fee']) -  $fields['discount'];
        $fields['total_pay'] = $total_pay < 0 ? 0 : $total_pay;
        $fields['user'] = $user->id;
        $transaction = Transaction::create($fields);
        $fieldsTrans['invoice'] = 'INV'.$date.$user->id.$transaction->id;
        $transaction->fill($fieldsTrans);
        $transaction->save();

        foreach ($cartData as $key => $prod) {

            $product = Product::where('id', $prod['product'])->where('status', '1')->first();
            if($product){
                $detailField['transaction']= $transaction->id;
                $detailField['product']= $product->id;
                $detailField['name']= $product->name;
                $detailField['description']= $product->description;
                $detailField['price']= $product->price;
                $detailField['image']= $product->image;
                $detailField['qty']= $prod['qty'];
                $detailField['total']= $prod['qty']* $product->price;
                TransactionDetail::create($detailField);

                $fieldProd['qty']= $product->qty - $prod['qty'];
                $product->fill($fieldProd);
                $product->save();

            }
        }
        Cart::where('user', $user->id)->delete();
        $data=Transaction::findOrFail($transaction->id);
        $data['product']= TransactionDetail::where('transaction', $transaction->id)->get();
        $data['user']= $data->user();
        $data['payment']= $data->payment();
        $data['delivery']= $data->delivery();
        $data['seller']= $data->seller();

        $message = "checkout success";
        return $this->respondCreated($data, $message);
    }

    /**
     * Get list Purchase (for Buyer)
     *
     * @group Transaction
     * @authenticated
     * @queryParam status string buying status [all,pending payment,cancel,process,send,waiting admin confirmation,done]. Example:all
     */

    public function purchase(Request $request){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $status = $request->has('status') ? $request->get('status') : 'all';
        $transaction = Transaction::query();
        if ($status != 'all') {
            $transaction->where('status', $status);
        }
        $transaction->where('user', $user->id);
        $transaction->orderBy('id','DESC');
        $result = $transaction->get();
        $total = $transaction->count();
        $data = $result->map(function ($item) {
            $item['seller'] = $item->seller();
            $item['product'] = TransactionDetail::where('transaction', $item->id)->get();
            $item['payment'] = $item->payment();
            $item['delivery'] = $item->delivery();
            return $item;
        });
        $res['message'] = 'data found';
        $res['total'] = $total;
        $res['data'] = $data;
        return $this->successResponse($res);
    }

    /**
     * Get list Order (for Seller)
     *
     * @group Transaction
     * @authenticated
     * @queryParam status string buying status [all,pending payment,cancel,process,send,waiting admin confirmation,done]. Example:all
     */
    public function order(Request $request){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $status = $request->has('status') ? $request->get('status') : 'all';
        $transaction = Transaction::query();
        if ($status != 'all') {
            $transaction->where('status', $status);
        }
        $transaction->where('seller', $user->id);
        $transaction->orderBy('id','DESC');
        $result = $transaction->get();
        $total = $transaction->count();
        $data = $result->map(function ($item) {
            $item['user'] = $item->user();
            $item['product'] = TransactionDetail::where('transaction', $item->id)->get();
            $item['payment'] = $item->payment();
            $item['delivery'] = $item->delivery();
            return $item;
        });
        $res['message'] = 'data found';
        $res['total'] = $total;
        $res['data'] = $data;
        return $this->successResponse($res);
    }

    /**
     * Get list Transaction (for Admin)
     *
     * @group Transaction
     * @authenticated
     * @queryParam status string buying status [all,pending payment,cancel,process,send,waiting admin confirmation,done]. Example:all
     */
    public function adminTransactionList(Request $request){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        if($user->role !=1){
            return $this->failNotFound();
        }
        $status = $request->has('status') ? $request->get('status') : 'all';
        $transaction = Transaction::query();
        if ($status != 'all') {
            $transaction->where('status', $status);
        }
        $transaction->orderBy('id','DESC');
        $result = $transaction->get();
        $total = $transaction->count();
        $data = $result->map(function ($item) {
            $item['user'] = $item->user();
            $item['product'] = TransactionDetail::where('transaction', $item->id)->get();
            $item['payment'] = $item->payment();
            $item['delivery'] = $item->delivery();
            return $item;
        });
        $res['message'] = 'data found';
        $res['total'] = $total;
        $res['data'] = $data;
        return $this->successResponse($res);
    }

    /**
     * Confirm Order by Id (for Seller)
     *
     * Confirm order by id transaction by seller. this process can be done only transaction status="pending payment".
     * this process will change transaction status from "pending payment" to "process".
     * @group Transaction
     * @authenticated
     * @urlParam id int required ID transaction.
     */

    public function confirmOrder(Request $request,$id){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $transaction = Transaction::findOrFail($id);
        if($transaction->seller != $user->id){
            return $this->failNotFound('transaction not found');
        }
        if($transaction->status != 'pending payment'){
            return $this->failNotFound('transaction cannot confirm');
        }
        $field['status']='process';
        $transaction->fill($field);
        $transaction->save();
        $res['message'] = 'transaction success confirmation';
        return $this->successResponse($res);
    }

    /**
     * Cancel Order by Id (for Seller)
     *
     * Cancel order by id transaction by seller. this process can be done only transaction status="pending payment".
     * this process will change transaction status from "pending payment" to "cancel" and cannot revert.
     * this process will add product qty in products.
     * @group Transaction
     * @authenticated
     * @urlParam id int required ID transaction.
     */

    public function sellerCancel(Request $request,$id){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $transaction = Transaction::findOrFail($id);
        if($transaction->seller != $user->id){
            return $this->failNotFound('transaction not found');
        }
        if($transaction->status != 'pending payment'){
            return $this->failNotFound('transaction cannot cancel');
        }
        $field['status']='cancel';
        $transaction->fill($field);
        $transaction->save();
        $transactionDetail=TransactionDetail::where('transaction',$id)->get();
        foreach ($transactionDetail as $key => $trans) {
            $product=Product::where('id', $trans['product'])->first();
            if($product){
                $prodFill['qty']= $product->qty + $trans['qty'];
                $product->fill($prodFill);
                $product->save();
            }
        }
        $res['message'] = 'transaction success cancel';
        return $this->successResponse($res);
    }

    /**
     * Send Order by Id (for Seller)
     *
     * Send order by id transaction by seller. this process can be done only transaction status="process".
     * this process will change transaction status from "process" to "send" and cannot revert.
     * @group Transaction
     * @authenticated
     * @urlParam id int required ID transaction.
     * @bodyParam no_resi string required No Resi.
     */

    public function sellerSend(Request $request,$id){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $validator = Validator::make($request->all(), [
            'no_resi' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->failValidationError($validator->errors());
        }
        $transaction = Transaction::findOrFail($id);
        if($transaction->seller != $user->id){
            return $this->failNotFound('transaction not found');
        }
        if($transaction->status != 'process'){
            return $this->failNotFound('transaction cannot send');
        }
        $field['status']='send';
        $field['no_resi']= 'no_resi';
        $transaction->fill($field);
        $transaction->save();
        $res['message'] = 'transaction success send';
        return $this->successResponse($res);
    }


    /**
     * Deliver Order by Id (for Buyer)
     *
     * Deliver order by id transaction by buyer. this process can be done only transaction status="send".
     * this process will change transaction status from "send" to "waiting admin confirmation" and cannot revert.
     * @group Transaction
     * @authenticated
     * @urlParam id int required ID transaction.
     */

    //buyer deliver
    public function updateDelivery(Request $request,$id){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $transaction = Transaction::findOrFail($id);
        if($transaction->user != $user->id){
            return $this->failNotFound('transaction not found');
        }
        if($transaction->status != 'send'){
            return $this->failNotFound('transaction cannot done');
        }
        $field['status']= 'waiting admin confirmation';
        $transaction->fill($field);
        $transaction->save();
        $res['message'] = 'transaction success deliveried';
        return $this->successResponse($res);
    }

    /**
     * Admin Confirm Order by Id (for admin)
     *
     * Deliver order by id transaction by admin. this process can be done only transaction status="waiting admin confirmation".
     * this process will change transaction status from "send" to "done" and cannot revert.
     * only admin can access and confirm
     * @group Transaction
     * @authenticated
     * @urlParam id int required ID transaction.
     */

    //buyer deliver
    public function adminConfirmDelivery(Request $request,$id){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $transaction = Transaction::findOrFail($id);
        if($user->role != 1){
            return $this->failNotFound('transaction not found');
        }
        if($transaction->status != 'waiting admin confirmation'){
            return $this->failNotFound('transaction cannot done');
        }
        $field['status']='done';
        $transaction->fill($field);
        $transaction->save();
        $res['message'] = 'transaction success confimation';
        return $this->successResponse($res);
    }

    /**
     * Cancel Order by Id (for buyer)
     *
     * Cancel order by id transaction by buyer. this process can be done only transaction status="pending payment".
     * this process will change transaction status from "pending payment" to "cancel" and cannot revert.
     * this process will add product qty in products.
     * @group Transaction
     * @authenticated
     * @urlParam id int required ID transaction.
     */
    //buyer cancel
    public function buyerCancel(Request $request,$id){
        $user = Auth::user();
        if (!$user) {
            return $this->failNotFound();
        }
        $transaction = Transaction::findOrFail($id);
        if($transaction->user != $user->id){
            return $this->failNotFound('transaction not found');
        }
        if($transaction->status != 'pending payment'){
            return $this->failNotFound('transaction cannot cancel');
        }
        $field['status']='cancel';
        $transaction->fill($field);
        $transaction->save();
        $transactionDetail = TransactionDetail::where('transaction', $id)->get();
        foreach ($transactionDetail as $key => $trans) {
            $product = Product::where('id', $trans['product'])->first();
            if ($product) {
                $prodFill['qty'] = $product->qty + $trans['qty'];
                $product->fill($prodFill);
                $product->save();
            }
        }
        $res['message'] = 'transaction success cancel';
        return $this->successResponse($res);
    }
}
