@php
    $page_title=$title ?? '';
@endphp
@extends('admin.layout.app')
@section('title', $page_title)

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('AppDashboard') }}"> {{__('admin.dashboard') }}</a></li>
<li class="breadcrumb-item active">{{$page_title}}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('admin.settings.navigation',['tab'=>'status'])
        <div class="card mb-3">
            <form action="{{ route('settings.managesite') }}"  enctype="multipart/form-data" method="POST">
             @csrf
            <div class="card-body">
                {!!Helpers::displayAlert()!!}
                <div class="row">
                    <div class="form-group col-md-5 col-sm-12">
                        <label class="h6 mb-3">{{ __('lang.site_status') }} :</label>
                        <div class="form-group m-checkbox-inline mb-0 custom-radio-ml row">
                            <div class="col radio radio-success">
                                <input id="site_status1" type="radio" name="site_status" value="0" data-original-title="" title="" {{ (!$maintenance ? "checked":"") }}>
                                <label class="mb-0" for="site_status1">{{ __('lang.active') }}</label>
                            </div>
                            <div class="col radio radio-success">
                                <input id="site_status2" type="radio" name="site_status" value="1" data-original-title="" title="" {{ ($maintenance ? "checked":"") }}>
                                <label class="mb-0" for="site_status2">{{ __('lang.inactive') }}</label>
                            </div>
                        </div>
                        @error('site_status')
                            <div class="invalid-feedback" >
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                        @can('update','settings')
                        <div class="form-footer mt-3">
                            <button type="submit" class="shadow-none  btn btn-primary">{{ __('lang.update') }}</button>
                        </div>
                        @endcan
                    </div>

                        </form>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end row -->
@endsection

