<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= \App\Models\User::factory()->createQuietly([
            'name' => 'admin',
            'email' => 'admin@site.com',
            'email_verified_at' => now(),
            'password' => Hash::make('admin'),
            'role' => '1',
            'status' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
